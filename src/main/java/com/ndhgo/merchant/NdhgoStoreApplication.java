package com.ndhgo.merchant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NdhgoStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(NdhgoStoreApplication.class, args);
	}

}
