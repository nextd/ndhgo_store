/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Akash Chakraborty
 *
 */
@Embeddable
public final class UniqueIndex {

    @Column(name="store_id", precision=10)
    private final int storeId;
    @Column(name="user_id", precision=10)
    private final int userId;

    /** Initializing constructor. */
    public UniqueIndex(int storeId, int userId) {
        this.storeId = storeId;
        this.userId = userId;
    }

    /** Private default constructor (for ORM frameworks). */
    @SuppressWarnings("unused")
    private UniqueIndex() {
        this(0, 0);
    }

    /**
     * Access method for storeId.
     *
     * @return the current value of storeId
     */
    public int getStoreId() {
        return storeId;
    }

    /**
     * Access method for userId.
     *
     * @return the current value of userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Compares this instance with another UniqueIndex.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (getClass() != other.getClass()) {
            return false;
        }
        UniqueIndex that = (UniqueIndex) other;
        if (this.getStoreId() != that.getStoreId()) {
            return false;
        }
        if (this.getUserId() != that.getUserId()) {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i = 1;
        int result = 17;
        i = getStoreId();
        result = 37*result + i;
        i = getUserId();
        result = 37*result + i;
        return result;
    }

}
