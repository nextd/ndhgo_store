/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * @author Akash Chakraborty
 *
 */
@Entity(name="marketplace_stores_notifications")
public class MarketplaceStoresNotifications implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 4735232039523230459L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="merchant_id", precision=10)
    private int merchantId;
    @Column(name="store_id", precision=10)
    private int storeId;
    @Column(name="notification_id", precision=10)
    private int notificationId;
    @Column(name="notification_keyword", length=200)
    private String notificationKeyword;
    @Column(precision=3)
    private Boolean enabled;
    @Column(name="is_promotional", precision=3)
    private short isPromotional;
    @Column(name="mail_notification", precision=3)
    private short mailNotification;
    @Column(name="sms_notification", precision=3)
    private short smsNotification;
    @Column(name="whatsapp_notification", precision=3)
    private short whatsappNotification;
    @Column(name="mail_templet")
    private String mailTemplet;
    @Column(name="sms_templet", length=200)
    private String smsTemplet;
    @Column(name="whatsapp_templet")
    private String whatsappTemplet;
    @Column(name="created_by", length=50)
    private String createdBy;
    @Column(name="created_at")
    private LocalDateTime createdAt;
    @Column(name="updated_at")
    private LocalDateTime updatedAt;
    @Column(name="updated_by", length=50)
    private String updatedBy;

    /** Default constructor. */
    public MarketplaceStoresNotifications() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for merchantId.
     *
     * @return the current value of merchantId
     */
    public int getMerchantId() {
        return merchantId;
    }

    /**
     * Setter method for merchantId.
     *
     * @param aMerchantId the new value for merchantId
     */
    public void setMerchantId(int aMerchantId) {
        merchantId = aMerchantId;
    }

    /**
     * Access method for storeId.
     *
     * @return the current value of storeId
     */
    public int getStoreId() {
        return storeId;
    }

    /**
     * Setter method for storeId.
     *
     * @param aStoreId the new value for storeId
     */
    public void setStoreId(int aStoreId) {
        storeId = aStoreId;
    }

    /**
     * Access method for notificationId.
     *
     * @return the current value of notificationId
     */
    public int getNotificationId() {
        return notificationId;
    }

    /**
     * Setter method for notificationId.
     *
     * @param aNotificationId the new value for notificationId
     */
    public void setNotificationId(int aNotificationId) {
        notificationId = aNotificationId;
    }

    /**
     * Access method for notificationKeyword.
     *
     * @return the current value of notificationKeyword
     */
    public String getNotificationKeyword() {
        return notificationKeyword;
    }

    /**
     * Setter method for notificationKeyword.
     *
     * @param aNotificationKeyword the new value for notificationKeyword
     */
    public void setNotificationKeyword(String aNotificationKeyword) {
        notificationKeyword = aNotificationKeyword;
    }

    /**
     * Access method for enabled.
     *
     * @return the current value of enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Setter method for enabled.
     *
     * @param aEnabled the new value for enabled
     */
    public void setEnabled(Boolean aEnabled) {
        enabled = aEnabled;
    }

    /**
     * Access method for isPromotional.
     *
     * @return the current value of isPromotional
     */
    public short getIsPromotional() {
        return isPromotional;
    }

    /**
     * Setter method for isPromotional.
     *
     * @param aIsPromotional the new value for isPromotional
     */
    public void setIsPromotional(short aIsPromotional) {
        isPromotional = aIsPromotional;
    }

    /**
     * Access method for mailNotification.
     *
     * @return the current value of mailNotification
     */
    public short getMailNotification() {
        return mailNotification;
    }

    /**
     * Setter method for mailNotification.
     *
     * @param aMailNotification the new value for mailNotification
     */
    public void setMailNotification(short aMailNotification) {
        mailNotification = aMailNotification;
    }

    /**
     * Access method for smsNotification.
     *
     * @return the current value of smsNotification
     */
    public short getSmsNotification() {
        return smsNotification;
    }

    /**
     * Setter method for smsNotification.
     *
     * @param aSmsNotification the new value for smsNotification
     */
    public void setSmsNotification(short aSmsNotification) {
        smsNotification = aSmsNotification;
    }

    /**
     * Access method for whatsappNotification.
     *
     * @return the current value of whatsappNotification
     */
    public short getWhatsappNotification() {
        return whatsappNotification;
    }

    /**
     * Setter method for whatsappNotification.
     *
     * @param aWhatsappNotification the new value for whatsappNotification
     */
    public void setWhatsappNotification(short aWhatsappNotification) {
        whatsappNotification = aWhatsappNotification;
    }

    /**
     * Access method for mailTemplet.
     *
     * @return the current value of mailTemplet
     */
    public String getMailTemplet() {
        return mailTemplet;
    }

    /**
     * Setter method for mailTemplet.
     *
     * @param aMailTemplet the new value for mailTemplet
     */
    public void setMailTemplet(String aMailTemplet) {
        mailTemplet = aMailTemplet;
    }

    /**
     * Access method for smsTemplet.
     *
     * @return the current value of smsTemplet
     */
    public String getSmsTemplet() {
        return smsTemplet;
    }

    /**
     * Setter method for smsTemplet.
     *
     * @param aSmsTemplet the new value for smsTemplet
     */
    public void setSmsTemplet(String aSmsTemplet) {
        smsTemplet = aSmsTemplet;
    }

    /**
     * Access method for whatsappTemplet.
     *
     * @return the current value of whatsappTemplet
     */
    public String getWhatsappTemplet() {
        return whatsappTemplet;
    }

    /**
     * Setter method for whatsappTemplet.
     *
     * @param aWhatsappTemplet the new value for whatsappTemplet
     */
    public void setWhatsappTemplet(String aWhatsappTemplet) {
        whatsappTemplet = aWhatsappTemplet;
    }

    /**
     * Access method for createdBy.
     *
     * @return the current value of createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter method for createdBy.
     *
     * @param aCreatedBy the new value for createdBy
     */
    public void setCreatedBy(String aCreatedBy) {
        createdBy = aCreatedBy;
    }

    /**
     * Access method for createdAt.
     *
     * @return the current value of createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * Setter method for createdAt.
     *
     * @param aCreatedAt the new value for createdAt
     */
    public void setCreatedAt(LocalDateTime aCreatedAt) {
        createdAt = aCreatedAt;
    }

    /**
     * Access method for updatedAt.
     *
     * @return the current value of updatedAt
     */
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Setter method for updatedAt.
     *
     * @param aUpdatedAt the new value for updatedAt
     */
    public void setUpdatedAt(LocalDateTime aUpdatedAt) {
        updatedAt = aUpdatedAt;
    }

    /**
     * Access method for updatedBy.
     *
     * @return the current value of updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Setter method for updatedBy.
     *
     * @param aUpdatedBy the new value for updatedBy
     */
    public void setUpdatedBy(String aUpdatedBy) {
        updatedBy = aUpdatedBy;
    }

    /**
     * Compares the key for this instance with another MarketplaceStoresNotifications.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MarketplaceStoresNotifications and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MarketplaceStoresNotifications)) {
            return false;
        }
        MarketplaceStoresNotifications that = (MarketplaceStoresNotifications) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MarketplaceStoresNotifications.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MarketplaceStoresNotifications)) return false;
        return this.equalKeys(other) && ((MarketplaceStoresNotifications)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MarketplaceStoresNotifications |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
