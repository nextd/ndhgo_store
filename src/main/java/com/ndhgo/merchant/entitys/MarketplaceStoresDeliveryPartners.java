/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;


/**
 * @author Akash Chakraborty
 *
 */
@Entity(name="marketplace_stores_delivery_partners")
public class MarketplaceStoresDeliveryPartners implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 297490595120940145L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="store_id", precision=10)
    private int storeId;
    @Column(name="delivery_partner", length=100)
    private String deliveryPartner;
    @Column(precision=5)
    private short priority;

    /** Default constructor. */
    public MarketplaceStoresDeliveryPartners() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for storeId.
     *
     * @return the current value of storeId
     */
    public int getStoreId() {
        return storeId;
    }

    /**
     * Setter method for storeId.
     *
     * @param aStoreId the new value for storeId
     */
    public void setStoreId(int aStoreId) {
        storeId = aStoreId;
    }

    /**
     * Access method for deliveryPartner.
     *
     * @return the current value of deliveryPartner
     */
    public String getDeliveryPartner() {
        return deliveryPartner;
    }

    /**
     * Setter method for deliveryPartner.
     *
     * @param aDeliveryPartner the new value for deliveryPartner
     */
    public void setDeliveryPartner(String aDeliveryPartner) {
        deliveryPartner = aDeliveryPartner;
    }

    /**
     * Access method for priority.
     *
     * @return the current value of priority
     */
    public short getPriority() {
        return priority;
    }

    /**
     * Setter method for priority.
     *
     * @param aPriority the new value for priority
     */
    public void setPriority(short aPriority) {
        priority = aPriority;
    }

    /**
     * Compares the key for this instance with another MarketplaceStoresDeliveryPartners.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MarketplaceStoresDeliveryPartners and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MarketplaceStoresDeliveryPartners)) {
            return false;
        }
        MarketplaceStoresDeliveryPartners that = (MarketplaceStoresDeliveryPartners) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MarketplaceStoresDeliveryPartners.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MarketplaceStoresDeliveryPartners)) return false;
        return this.equalKeys(other) && ((MarketplaceStoresDeliveryPartners)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MarketplaceStoresDeliveryPartners |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
