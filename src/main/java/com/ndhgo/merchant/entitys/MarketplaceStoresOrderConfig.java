/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * @author Akash Chakraborty
 *
 */
@Entity(name="marketplace_stores_order_config")
public class MarketplaceStoresOrderConfig implements Serializable {

    /**
	 * serial Version for the object
	 */
	private static final long serialVersionUID = -8188762741734957493L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="store_id", precision=10)
    private int storeId;
    @Column(name="invoice_logo_url")
    private String invoiceLogoUrl;
    @Column(name="marketplace_stores_order_configcol", length=45)
    private String marketplaceStoresOrderConfigcol;
    @Column(name="company_gstn", length=50)
    private String companyGstn;
    @Column(name="invoice_no", precision=10)
    private int invoiceNo;
    @Column(name="invoice_prefix", length=20)
    private String invoicePrefix;
    @Column(name="order_no", precision=10)
    private int orderNo;
    @Column(name="order_prefix", length=20)
    private String orderPrefix;
    @Column(name="terms_condition")
    private String termsCondition;
    @Column(name="signature_url")
    private String signatureUrl;
    @Column(name="created_by", length=50)
    private String createdBy;
    @Column(name="created_at", nullable=false)
    private LocalDateTime createdAt;
    @Column(name="updated_at")
    private LocalDateTime updatedAt;
    @Column(name="updated_by", length=50)
    private String updatedBy;

    /** Default constructor. */
    public MarketplaceStoresOrderConfig() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for storeId.
     *
     * @return the current value of storeId
     */
    public int getStoreId() {
        return storeId;
    }

    /**
     * Setter method for storeId.
     *
     * @param aStoreId the new value for storeId
     */
    public void setStoreId(int aStoreId) {
        storeId = aStoreId;
    }

    /**
     * Access method for invoiceLogoUrl.
     *
     * @return the current value of invoiceLogoUrl
     */
    public String getInvoiceLogoUrl() {
        return invoiceLogoUrl;
    }

    /**
     * Setter method for invoiceLogoUrl.
     *
     * @param aInvoiceLogoUrl the new value for invoiceLogoUrl
     */
    public void setInvoiceLogoUrl(String aInvoiceLogoUrl) {
        invoiceLogoUrl = aInvoiceLogoUrl;
    }

    /**
     * Access method for marketplaceStoresOrderConfigcol.
     *
     * @return the current value of marketplaceStoresOrderConfigcol
     */
    public String getMarketplaceStoresOrderConfigcol() {
        return marketplaceStoresOrderConfigcol;
    }

    /**
     * Setter method for marketplaceStoresOrderConfigcol.
     *
     * @param aMarketplaceStoresOrderConfigcol the new value for marketplaceStoresOrderConfigcol
     */
    public void setMarketplaceStoresOrderConfigcol(String aMarketplaceStoresOrderConfigcol) {
        marketplaceStoresOrderConfigcol = aMarketplaceStoresOrderConfigcol;
    }

    /**
     * Access method for companyGstn.
     *
     * @return the current value of companyGstn
     */
    public String getCompanyGstn() {
        return companyGstn;
    }

    /**
     * Setter method for companyGstn.
     *
     * @param aCompanyGstn the new value for companyGstn
     */
    public void setCompanyGstn(String aCompanyGstn) {
        companyGstn = aCompanyGstn;
    }

    /**
     * Access method for invoiceNo.
     *
     * @return the current value of invoiceNo
     */
    public int getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * Setter method for invoiceNo.
     *
     * @param aInvoiceNo the new value for invoiceNo
     */
    public void setInvoiceNo(int aInvoiceNo) {
        invoiceNo = aInvoiceNo;
    }

    /**
     * Access method for invoicePrefix.
     *
     * @return the current value of invoicePrefix
     */
    public String getInvoicePrefix() {
        return invoicePrefix;
    }

    /**
     * Setter method for invoicePrefix.
     *
     * @param aInvoicePrefix the new value for invoicePrefix
     */
    public void setInvoicePrefix(String aInvoicePrefix) {
        invoicePrefix = aInvoicePrefix;
    }

    /**
     * Access method for orderNo.
     *
     * @return the current value of orderNo
     */
    public int getOrderNo() {
        return orderNo;
    }

    /**
     * Setter method for orderNo.
     *
     * @param aOrderNo the new value for orderNo
     */
    public void setOrderNo(int aOrderNo) {
        orderNo = aOrderNo;
    }

    /**
     * Access method for orderPrefix.
     *
     * @return the current value of orderPrefix
     */
    public String getOrderPrefix() {
        return orderPrefix;
    }

    /**
     * Setter method for orderPrefix.
     *
     * @param aOrderPrefix the new value for orderPrefix
     */
    public void setOrderPrefix(String aOrderPrefix) {
        orderPrefix = aOrderPrefix;
    }

    /**
     * Access method for termsCondition.
     *
     * @return the current value of termsCondition
     */
    public String getTermsCondition() {
        return termsCondition;
    }

    /**
     * Setter method for termsCondition.
     *
     * @param aTermsCondition the new value for termsCondition
     */
    public void setTermsCondition(String aTermsCondition) {
        termsCondition = aTermsCondition;
    }

    /**
     * Access method for signatureUrl.
     *
     * @return the current value of signatureUrl
     */
    public String getSignatureUrl() {
        return signatureUrl;
    }

    /**
     * Setter method for signatureUrl.
     *
     * @param aSignatureUrl the new value for signatureUrl
     */
    public void setSignatureUrl(String aSignatureUrl) {
        signatureUrl = aSignatureUrl;
    }

    /**
     * Access method for createdBy.
     *
     * @return the current value of createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter method for createdBy.
     *
     * @param aCreatedBy the new value for createdBy
     */
    public void setCreatedBy(String aCreatedBy) {
        createdBy = aCreatedBy;
    }

    /**
     * Access method for createdAt.
     *
     * @return the current value of createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * Setter method for createdAt.
     *
     * @param aCreatedAt the new value for createdAt
     */
    public void setCreatedAt(LocalDateTime aCreatedAt) {
        createdAt = aCreatedAt;
    }

    /**
     * Access method for updatedAt.
     *
     * @return the current value of updatedAt
     */
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Setter method for updatedAt.
     *
     * @param aUpdatedAt the new value for updatedAt
     */
    public void setUpdatedAt(LocalDateTime aUpdatedAt) {
        updatedAt = aUpdatedAt;
    }

    /**
     * Access method for updatedBy.
     *
     * @return the current value of updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Setter method for updatedBy.
     *
     * @param aUpdatedBy the new value for updatedBy
     */
    public void setUpdatedBy(String aUpdatedBy) {
        updatedBy = aUpdatedBy;
    }

    /**
     * Compares the key for this instance with another MarketplaceStoresOrderConfig.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MarketplaceStoresOrderConfig and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MarketplaceStoresOrderConfig)) {
            return false;
        }
        MarketplaceStoresOrderConfig that = (MarketplaceStoresOrderConfig) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MarketplaceStoresOrderConfig.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MarketplaceStoresOrderConfig)) return false;
        return this.equalKeys(other) && ((MarketplaceStoresOrderConfig)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MarketplaceStoresOrderConfig |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
