/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;


/**
 * @author Akash Chakraborty
 *
 */
@Entity(name="merchant_bank_details")
public class MerchantBankDetails implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7009256512200322337L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="account_id", precision=10)
    private int accountId;
    @Column(name="legal_name", length=200)
    private String legalName;
    @Column(name="benificiary_name", length=250)
    private String benificiaryName;
    @Column(name="account_number", length=25)
    private String accountNumber;
    @Column(name="bank_linked_mobile", length=10)
    private String bankLinkedMobile;
    @Column(name="account_type", length=7)
    private String accountType;
    @Column(name="bank_name", length=100)
    private String bankName;
    @Column(name="branch_name", length=100)
    private String branchName;
    @Column(name="bank_address")
    private String bankAddress;
    @Column(name="ifsc_code", length=15)
    private String ifscCode;
    @Column(name="swift_code", length=20)
    private String swiftCode;
    @Column(name="account_verified", length=3)
    private boolean accountVerified;
    @Column(name="account_verification_reference", length=100)
    private String accountVerificationReference;
    @Column(name="bank_response")
    private String bankResponse;
    @Column(name="pan_no", length=12)
    private String panNo;
    @Column(name="pan_full_name", length=200)
    private String panFullName;
    @Column(name="pan_verified", length=3)
    private boolean panVerified;
    @Column(name="pan_verification_reference", length=100)
    private String panVerificationReference;
    @Column(name="pan_response")
    private String panResponse;
    @Column(name="business_code", length=50)
    private String businessCode;
    @Column(name="manual_verified", length=3)
    private boolean manualVerified;
    @Column(name="maual_verification_by", length=100)
    private String maualVerificationBy;
    @Column(name="manual_verification_remarks", length=200)
    private String manualVerificationRemarks;
    @Column(name="created_by", length=50)
    private String createdBy;
    @Column(name="created_at")
    private LocalDateTime createdAt;
    @Column(name="updated_by", length=50)
    private String updatedBy;
    @Column(name="updated_at")
    private LocalDateTime updatedAt;

    /** Default constructor. */
    public MerchantBankDetails() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for accountId.
     *
     * @return the current value of accountId
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Setter method for accountId.
     *
     * @param aAccountId the new value for accountId
     */
    public void setAccountId(int aAccountId) {
        accountId = aAccountId;
    }

    /**
     * Access method for legalName.
     *
     * @return the current value of legalName
     */
    public String getLegalName() {
        return legalName;
    }

    /**
     * Setter method for legalName.
     *
     * @param aLegalName the new value for legalName
     */
    public void setLegalName(String aLegalName) {
        legalName = aLegalName;
    }

    /**
     * Access method for benificiaryName.
     *
     * @return the current value of benificiaryName
     */
    public String getBenificiaryName() {
        return benificiaryName;
    }

    /**
     * Setter method for benificiaryName.
     *
     * @param aBenificiaryName the new value for benificiaryName
     */
    public void setBenificiaryName(String aBenificiaryName) {
        benificiaryName = aBenificiaryName;
    }

    /**
     * Access method for accountNumber.
     *
     * @return the current value of accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Setter method for accountNumber.
     *
     * @param aAccountNumber the new value for accountNumber
     */
    public void setAccountNumber(String aAccountNumber) {
        accountNumber = aAccountNumber;
    }

    /**
     * Access method for bankLinkedMobile.
     *
     * @return the current value of bankLinkedMobile
     */
    public String getBankLinkedMobile() {
        return bankLinkedMobile;
    }

    /**
     * Setter method for bankLinkedMobile.
     *
     * @param aBankLinkedMobile the new value for bankLinkedMobile
     */
    public void setBankLinkedMobile(String aBankLinkedMobile) {
        bankLinkedMobile = aBankLinkedMobile;
    }

    /**
     * Access method for accountType.
     *
     * @return the current value of accountType
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Setter method for accountType.
     *
     * @param aAccountType the new value for accountType
     */
    public void setAccountType(String aAccountType) {
        accountType = aAccountType;
    }

    /**
     * Access method for bankName.
     *
     * @return the current value of bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * Setter method for bankName.
     *
     * @param aBankName the new value for bankName
     */
    public void setBankName(String aBankName) {
        bankName = aBankName;
    }

    /**
     * Access method for branchName.
     *
     * @return the current value of branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * Setter method for branchName.
     *
     * @param aBranchName the new value for branchName
     */
    public void setBranchName(String aBranchName) {
        branchName = aBranchName;
    }

    /**
     * Access method for bankAddress.
     *
     * @return the current value of bankAddress
     */
    public String getBankAddress() {
        return bankAddress;
    }

    /**
     * Setter method for bankAddress.
     *
     * @param aBankAddress the new value for bankAddress
     */
    public void setBankAddress(String aBankAddress) {
        bankAddress = aBankAddress;
    }

    /**
     * Access method for ifscCode.
     *
     * @return the current value of ifscCode
     */
    public String getIfscCode() {
        return ifscCode;
    }

    /**
     * Setter method for ifscCode.
     *
     * @param aIfscCode the new value for ifscCode
     */
    public void setIfscCode(String aIfscCode) {
        ifscCode = aIfscCode;
    }

    /**
     * Access method for swiftCode.
     *
     * @return the current value of swiftCode
     */
    public String getSwiftCode() {
        return swiftCode;
    }

    /**
     * Setter method for swiftCode.
     *
     * @param aSwiftCode the new value for swiftCode
     */
    public void setSwiftCode(String aSwiftCode) {
        swiftCode = aSwiftCode;
    }

    /**
     * Access method for accountVerified.
     *
     * @return true if and only if accountVerified is currently true
     */
    public boolean getAccountVerified() {
        return accountVerified;
    }

    /**
     * Setter method for accountVerified.
     *
     * @param aAccountVerified the new value for accountVerified
     */
    public void setAccountVerified(boolean aAccountVerified) {
        accountVerified = aAccountVerified;
    }

    /**
     * Access method for accountVerificationReference.
     *
     * @return the current value of accountVerificationReference
     */
    public String getAccountVerificationReference() {
        return accountVerificationReference;
    }

    /**
     * Setter method for accountVerificationReference.
     *
     * @param aAccountVerificationReference the new value for accountVerificationReference
     */
    public void setAccountVerificationReference(String aAccountVerificationReference) {
        accountVerificationReference = aAccountVerificationReference;
    }

    /**
     * Access method for bankResponse.
     *
     * @return the current value of bankResponse
     */
    public String getBankResponse() {
        return bankResponse;
    }

    /**
     * Setter method for bankResponse.
     *
     * @param aBankResponse the new value for bankResponse
     */
    public void setBankResponse(String aBankResponse) {
        bankResponse = aBankResponse;
    }

    /**
     * Access method for panNo.
     *
     * @return the current value of panNo
     */
    public String getPanNo() {
        return panNo;
    }

    /**
     * Setter method for panNo.
     *
     * @param aPanNo the new value for panNo
     */
    public void setPanNo(String aPanNo) {
        panNo = aPanNo;
    }

    /**
     * Access method for panFullName.
     *
     * @return the current value of panFullName
     */
    public String getPanFullName() {
        return panFullName;
    }

    /**
     * Setter method for panFullName.
     *
     * @param aPanFullName the new value for panFullName
     */
    public void setPanFullName(String aPanFullName) {
        panFullName = aPanFullName;
    }

    /**
     * Access method for panVerified.
     *
     * @return true if and only if panVerified is currently true
     */
    public boolean getPanVerified() {
        return panVerified;
    }

    /**
     * Setter method for panVerified.
     *
     * @param aPanVerified the new value for panVerified
     */
    public void setPanVerified(boolean aPanVerified) {
        panVerified = aPanVerified;
    }

    /**
     * Access method for panVerificationReference.
     *
     * @return the current value of panVerificationReference
     */
    public String getPanVerificationReference() {
        return panVerificationReference;
    }

    /**
     * Setter method for panVerificationReference.
     *
     * @param aPanVerificationReference the new value for panVerificationReference
     */
    public void setPanVerificationReference(String aPanVerificationReference) {
        panVerificationReference = aPanVerificationReference;
    }

    /**
     * Access method for panResponse.
     *
     * @return the current value of panResponse
     */
    public String getPanResponse() {
        return panResponse;
    }

    /**
     * Setter method for panResponse.
     *
     * @param aPanResponse the new value for panResponse
     */
    public void setPanResponse(String aPanResponse) {
        panResponse = aPanResponse;
    }

    /**
     * Access method for businessCode.
     *
     * @return the current value of businessCode
     */
    public String getBusinessCode() {
        return businessCode;
    }

    /**
     * Setter method for businessCode.
     *
     * @param aBusinessCode the new value for businessCode
     */
    public void setBusinessCode(String aBusinessCode) {
        businessCode = aBusinessCode;
    }

    /**
     * Access method for manualVerified.
     *
     * @return true if and only if manualVerified is currently true
     */
    public boolean getManualVerified() {
        return manualVerified;
    }

    /**
     * Setter method for manualVerified.
     *
     * @param aManualVerified the new value for manualVerified
     */
    public void setManualVerified(boolean aManualVerified) {
        manualVerified = aManualVerified;
    }

    /**
     * Access method for maualVerificationBy.
     *
     * @return the current value of maualVerificationBy
     */
    public String getMaualVerificationBy() {
        return maualVerificationBy;
    }

    /**
     * Setter method for maualVerificationBy.
     *
     * @param aMaualVerificationBy the new value for maualVerificationBy
     */
    public void setMaualVerificationBy(String aMaualVerificationBy) {
        maualVerificationBy = aMaualVerificationBy;
    }

    /**
     * Access method for manualVerificationRemarks.
     *
     * @return the current value of manualVerificationRemarks
     */
    public String getManualVerificationRemarks() {
        return manualVerificationRemarks;
    }

    /**
     * Setter method for manualVerificationRemarks.
     *
     * @param aManualVerificationRemarks the new value for manualVerificationRemarks
     */
    public void setManualVerificationRemarks(String aManualVerificationRemarks) {
        manualVerificationRemarks = aManualVerificationRemarks;
    }

    /**
     * Access method for createdBy.
     *
     * @return the current value of createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter method for createdBy.
     *
     * @param aCreatedBy the new value for createdBy
     */
    public void setCreatedBy(String aCreatedBy) {
        createdBy = aCreatedBy;
    }

    /**
     * Access method for createdAt.
     *
     * @return the current value of createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * Setter method for createdAt.
     *
     * @param aCreatedAt the new value for createdAt
     */
    public void setCreatedAt(LocalDateTime aCreatedAt) {
        createdAt = aCreatedAt;
    }

    /**
     * Access method for updatedBy.
     *
     * @return the current value of updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Setter method for updatedBy.
     *
     * @param aUpdatedBy the new value for updatedBy
     */
    public void setUpdatedBy(String aUpdatedBy) {
        updatedBy = aUpdatedBy;
    }

    /**
     * Access method for updatedAt.
     *
     * @return the current value of updatedAt
     */
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Setter method for updatedAt.
     *
     * @param aUpdatedAt the new value for updatedAt
     */
    public void setUpdatedAt(LocalDateTime aUpdatedAt) {
        updatedAt = aUpdatedAt;
    }

    /**
     * Compares the key for this instance with another MerchantBankDetails.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MerchantBankDetails and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MerchantBankDetails)) {
            return false;
        }
        MerchantBankDetails that = (MerchantBankDetails) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MerchantBankDetails.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MerchantBankDetails)) return false;
        return this.equalKeys(other) && ((MerchantBankDetails)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MerchantBankDetails |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
