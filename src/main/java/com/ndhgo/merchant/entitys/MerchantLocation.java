/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;



/**
 * @author Akash Chakraborty
 *
 */
@Entity(name="merchant_location")
public class MerchantLocation implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -172816745244016400L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="geo_lat", precision=22)
    private double geoLat;
    @Column(name="geo_long", precision=22)
    private double geoLong;
    @Column(name="short_address", length=200)
    private String shortAddress;
    @Column(length=250)
    private String address;
    @Column(length=100)
    private String landmark;
    @Column(length=100)
    private String city;
    @Column(name="region_name", length=100)
    private String regionName;
    @Column(name="region_code", length=20)
    private String regionCode;
    @Column(name="zip_code", length=25)
    private String zipCode;
    @Column(name="country_code", length=3)
    private String countryCode;
    @Column(name="created_by", length=50)
    private String createdBy;
    @Column(name="created_at")
    private LocalDateTime createdAt;
    @Column(name="updated_by", length=50)
    private String updatedBy;
    @Column(name="updated_at")
    private LocalDateTime updatedAt;
    @ManyToOne
    @JoinColumn(name="account_id")
    private MerchantAccount merchantAccount;

    /** Default constructor. */
    public MerchantLocation() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for geoLat.
     *
     * @return the current value of geoLat
     */
    public double getGeoLat() {
        return geoLat;
    }

    /**
     * Setter method for geoLat.
     *
     * @param aGeoLat the new value for geoLat
     */
    public void setGeoLat(double aGeoLat) {
        geoLat = aGeoLat;
    }

    /**
     * Access method for geoLong.
     *
     * @return the current value of geoLong
     */
    public double getGeoLong() {
        return geoLong;
    }

    /**
     * Setter method for geoLong.
     *
     * @param aGeoLong the new value for geoLong
     */
    public void setGeoLong(double aGeoLong) {
        geoLong = aGeoLong;
    }

    /**
     * Access method for shortAddress.
     *
     * @return the current value of shortAddress
     */
    public String getShortAddress() {
        return shortAddress;
    }

    /**
     * Setter method for shortAddress.
     *
     * @param aShortAddress the new value for shortAddress
     */
    public void setShortAddress(String aShortAddress) {
        shortAddress = aShortAddress;
    }

    /**
     * Access method for address.
     *
     * @return the current value of address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setter method for address.
     *
     * @param aAddress the new value for address
     */
    public void setAddress(String aAddress) {
        address = aAddress;
    }

    /**
     * Access method for landmark.
     *
     * @return the current value of landmark
     */
    public String getLandmark() {
        return landmark;
    }

    /**
     * Setter method for landmark.
     *
     * @param aLandmark the new value for landmark
     */
    public void setLandmark(String aLandmark) {
        landmark = aLandmark;
    }

    /**
     * Access method for city.
     *
     * @return the current value of city
     */
    public String getCity() {
        return city;
    }

    /**
     * Setter method for city.
     *
     * @param aCity the new value for city
     */
    public void setCity(String aCity) {
        city = aCity;
    }

    /**
     * Access method for regionName.
     *
     * @return the current value of regionName
     */
    public String getRegionName() {
        return regionName;
    }

    /**
     * Setter method for regionName.
     *
     * @param aRegionName the new value for regionName
     */
    public void setRegionName(String aRegionName) {
        regionName = aRegionName;
    }

    /**
     * Access method for regionCode.
     *
     * @return the current value of regionCode
     */
    public String getRegionCode() {
        return regionCode;
    }

    /**
     * Setter method for regionCode.
     *
     * @param aRegionCode the new value for regionCode
     */
    public void setRegionCode(String aRegionCode) {
        regionCode = aRegionCode;
    }

    /**
     * Access method for zipCode.
     *
     * @return the current value of zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * Setter method for zipCode.
     *
     * @param aZipCode the new value for zipCode
     */
    public void setZipCode(String aZipCode) {
        zipCode = aZipCode;
    }

    /**
     * Access method for countryCode.
     *
     * @return the current value of countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Setter method for countryCode.
     *
     * @param aCountryCode the new value for countryCode
     */
    public void setCountryCode(String aCountryCode) {
        countryCode = aCountryCode;
    }

    /**
     * Access method for createdBy.
     *
     * @return the current value of createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter method for createdBy.
     *
     * @param aCreatedBy the new value for createdBy
     */
    public void setCreatedBy(String aCreatedBy) {
        createdBy = aCreatedBy;
    }

    /**
     * Access method for createdAt.
     *
     * @return the current value of createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * Setter method for createdAt.
     *
     * @param aCreatedAt the new value for createdAt
     */
    public void setCreatedAt(LocalDateTime aCreatedAt) {
        createdAt = aCreatedAt;
    }

    /**
     * Access method for updatedBy.
     *
     * @return the current value of updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Setter method for updatedBy.
     *
     * @param aUpdatedBy the new value for updatedBy
     */
    public void setUpdatedBy(String aUpdatedBy) {
        updatedBy = aUpdatedBy;
    }

    /**
     * Access method for updatedAt.
     *
     * @return the current value of updatedAt
     */
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Setter method for updatedAt.
     *
     * @param aUpdatedAt the new value for updatedAt
     */
    public void setUpdatedAt(LocalDateTime aUpdatedAt) {
        updatedAt = aUpdatedAt;
    }

    /**
     * Access method for merchantAccount.
     *
     * @return the current value of merchantAccount
     */
    public MerchantAccount getMerchantAccount() {
        return merchantAccount;
    }

    /**
     * Setter method for merchantAccount.
     *
     * @param aMerchantAccount the new value for merchantAccount
     */
    public void setMerchantAccount(MerchantAccount aMerchantAccount) {
        merchantAccount = aMerchantAccount;
    }

    /**
     * Compares the key for this instance with another MerchantLocation.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MerchantLocation and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MerchantLocation)) {
            return false;
        }
        MerchantLocation that = (MerchantLocation) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MerchantLocation.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MerchantLocation)) return false;
        return this.equalKeys(other) && ((MerchantLocation)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MerchantLocation |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
