/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;



/**
 * @author Akash Chakraborty
 *
 */
@Entity(name="merchant_url")
public class MerchantUrl implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1872916474935801782L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="account_id", precision=10)
    private int accountId;
    @Column(name="brand_name", length=100)
    private String brandName;
    @Column(name="internal_protocol", length=8)
    private String internalProtocol;
    @Column(name="internal_sub_domain", length=100)
    private String internalSubDomain;
    @Column(name="internal_domain", length=200)
    private String internalDomain;
    @Column(name="internal_public_url", length=255)
    private String internalPublicUrl;
    @Column(name="external_protocol", length=8)
    private String externalProtocol;
    @Column(name="external_sub_domain", length=100)
    private String externalSubDomain;
    @Column(name="external_domain", length=100)
    private String externalDomain;
    @Column(name="external_public_url", length=255)
    private String externalPublicUrl;
    @Column(name="block_internal_url", length=3)
    private boolean blockInternalUrl;
    @Column(name="block_external_url", length=3)
    private boolean blockExternalUrl;
    @Column(name="theme_color", length=7)
    private String themeColor;
    @Column(name="theme_text_color", length=7)
    private String themeTextColor;
    @Column(name="default_store_id", precision=10)
    private int defaultStoreId;
    @Column(name="theme_id", precision=10)
    private int themeId;
    @Column(name="google_analytics_id", length=50)
    private String googleAnalyticsId;
    @Column(name="fb_pixel_id", length=50)
    private String fbPixelId;
    @Column(name="seo_title", length=100)
    private String seoTitle;
    @Column(name="seo_description", length=200)
    private String seoDescription;
    @Column(name="fav_icon_url")
    private String favIconUrl;
    @Column(name="logo_url")
    private String logoUrl;
    @Column(name="created_by", length=50)
    private String createdBy;
    @Column(name="created_at")
    private LocalDateTime createdAt;
    @Column(name="updated_by", length=50)
    private String updatedBy;
    @Column(name="updated_at")
    private LocalDateTime updatedAt;

    /** Default constructor. */
    public MerchantUrl() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for accountId.
     *
     * @return the current value of accountId
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Setter method for accountId.
     *
     * @param aAccountId the new value for accountId
     */
    public void setAccountId(int aAccountId) {
        accountId = aAccountId;
    }

    /**
     * Access method for brandName.
     *
     * @return the current value of brandName
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Setter method for brandName.
     *
     * @param aBrandName the new value for brandName
     */
    public void setBrandName(String aBrandName) {
        brandName = aBrandName;
    }

    /**
     * Access method for internalProtocol.
     *
     * @return the current value of internalProtocol
     */
    public String getInternalProtocol() {
        return internalProtocol;
    }

    /**
     * Setter method for internalProtocol.
     *
     * @param aInternalProtocol the new value for internalProtocol
     */
    public void setInternalProtocol(String aInternalProtocol) {
        internalProtocol = aInternalProtocol;
    }

    /**
     * Access method for internalSubDomain.
     *
     * @return the current value of internalSubDomain
     */
    public String getInternalSubDomain() {
        return internalSubDomain;
    }

    /**
     * Setter method for internalSubDomain.
     *
     * @param aInternalSubDomain the new value for internalSubDomain
     */
    public void setInternalSubDomain(String aInternalSubDomain) {
        internalSubDomain = aInternalSubDomain;
    }

    /**
     * Access method for internalDomain.
     *
     * @return the current value of internalDomain
     */
    public String getInternalDomain() {
        return internalDomain;
    }

    /**
     * Setter method for internalDomain.
     *
     * @param aInternalDomain the new value for internalDomain
     */
    public void setInternalDomain(String aInternalDomain) {
        internalDomain = aInternalDomain;
    }

    /**
     * Access method for internalPublicUrl.
     *
     * @return the current value of internalPublicUrl
     */
    public String getInternalPublicUrl() {
        return internalPublicUrl;
    }

    /**
     * Setter method for internalPublicUrl.
     *
     * @param aInternalPublicUrl the new value for internalPublicUrl
     */
    public void setInternalPublicUrl(String aInternalPublicUrl) {
        internalPublicUrl = aInternalPublicUrl;
    }

    /**
     * Access method for externalProtocol.
     *
     * @return the current value of externalProtocol
     */
    public String getExternalProtocol() {
        return externalProtocol;
    }

    /**
     * Setter method for externalProtocol.
     *
     * @param aExternalProtocol the new value for externalProtocol
     */
    public void setExternalProtocol(String aExternalProtocol) {
        externalProtocol = aExternalProtocol;
    }

    /**
     * Access method for externalSubDomain.
     *
     * @return the current value of externalSubDomain
     */
    public String getExternalSubDomain() {
        return externalSubDomain;
    }

    /**
     * Setter method for externalSubDomain.
     *
     * @param aExternalSubDomain the new value for externalSubDomain
     */
    public void setExternalSubDomain(String aExternalSubDomain) {
        externalSubDomain = aExternalSubDomain;
    }

    /**
     * Access method for externalDomain.
     *
     * @return the current value of externalDomain
     */
    public String getExternalDomain() {
        return externalDomain;
    }

    /**
     * Setter method for externalDomain.
     *
     * @param aExternalDomain the new value for externalDomain
     */
    public void setExternalDomain(String aExternalDomain) {
        externalDomain = aExternalDomain;
    }

    /**
     * Access method for externalPublicUrl.
     *
     * @return the current value of externalPublicUrl
     */
    public String getExternalPublicUrl() {
        return externalPublicUrl;
    }

    /**
     * Setter method for externalPublicUrl.
     *
     * @param aExternalPublicUrl the new value for externalPublicUrl
     */
    public void setExternalPublicUrl(String aExternalPublicUrl) {
        externalPublicUrl = aExternalPublicUrl;
    }

    /**
     * Access method for blockInternalUrl.
     *
     * @return true if and only if blockInternalUrl is currently true
     */
    public boolean getBlockInternalUrl() {
        return blockInternalUrl;
    }

    /**
     * Setter method for blockInternalUrl.
     *
     * @param aBlockInternalUrl the new value for blockInternalUrl
     */
    public void setBlockInternalUrl(boolean aBlockInternalUrl) {
        blockInternalUrl = aBlockInternalUrl;
    }

    /**
     * Access method for blockExternalUrl.
     *
     * @return true if and only if blockExternalUrl is currently true
     */
    public boolean getBlockExternalUrl() {
        return blockExternalUrl;
    }

    /**
     * Setter method for blockExternalUrl.
     *
     * @param aBlockExternalUrl the new value for blockExternalUrl
     */
    public void setBlockExternalUrl(boolean aBlockExternalUrl) {
        blockExternalUrl = aBlockExternalUrl;
    }

    /**
     * Access method for themeColor.
     *
     * @return the current value of themeColor
     */
    public String getThemeColor() {
        return themeColor;
    }

    /**
     * Setter method for themeColor.
     *
     * @param aThemeColor the new value for themeColor
     */
    public void setThemeColor(String aThemeColor) {
        themeColor = aThemeColor;
    }

    /**
     * Access method for themeTextColor.
     *
     * @return the current value of themeTextColor
     */
    public String getThemeTextColor() {
        return themeTextColor;
    }

    /**
     * Setter method for themeTextColor.
     *
     * @param aThemeTextColor the new value for themeTextColor
     */
    public void setThemeTextColor(String aThemeTextColor) {
        themeTextColor = aThemeTextColor;
    }

    /**
     * Access method for defaultStoreId.
     *
     * @return the current value of defaultStoreId
     */
    public int getDefaultStoreId() {
        return defaultStoreId;
    }

    /**
     * Setter method for defaultStoreId.
     *
     * @param aDefaultStoreId the new value for defaultStoreId
     */
    public void setDefaultStoreId(int aDefaultStoreId) {
        defaultStoreId = aDefaultStoreId;
    }

    /**
     * Access method for themeId.
     *
     * @return the current value of themeId
     */
    public int getThemeId() {
        return themeId;
    }

    /**
     * Setter method for themeId.
     *
     * @param aThemeId the new value for themeId
     */
    public void setThemeId(int aThemeId) {
        themeId = aThemeId;
    }

    /**
     * Access method for googleAnalyticsId.
     *
     * @return the current value of googleAnalyticsId
     */
    public String getGoogleAnalyticsId() {
        return googleAnalyticsId;
    }

    /**
     * Setter method for googleAnalyticsId.
     *
     * @param aGoogleAnalyticsId the new value for googleAnalyticsId
     */
    public void setGoogleAnalyticsId(String aGoogleAnalyticsId) {
        googleAnalyticsId = aGoogleAnalyticsId;
    }

    /**
     * Access method for fbPixelId.
     *
     * @return the current value of fbPixelId
     */
    public String getFbPixelId() {
        return fbPixelId;
    }

    /**
     * Setter method for fbPixelId.
     *
     * @param aFbPixelId the new value for fbPixelId
     */
    public void setFbPixelId(String aFbPixelId) {
        fbPixelId = aFbPixelId;
    }

    /**
     * Access method for seoTitle.
     *
     * @return the current value of seoTitle
     */
    public String getSeoTitle() {
        return seoTitle;
    }

    /**
     * Setter method for seoTitle.
     *
     * @param aSeoTitle the new value for seoTitle
     */
    public void setSeoTitle(String aSeoTitle) {
        seoTitle = aSeoTitle;
    }

    /**
     * Access method for seoDescription.
     *
     * @return the current value of seoDescription
     */
    public String getSeoDescription() {
        return seoDescription;
    }

    /**
     * Setter method for seoDescription.
     *
     * @param aSeoDescription the new value for seoDescription
     */
    public void setSeoDescription(String aSeoDescription) {
        seoDescription = aSeoDescription;
    }

    /**
     * Access method for favIconUrl.
     *
     * @return the current value of favIconUrl
     */
    public String getFavIconUrl() {
        return favIconUrl;
    }

    /**
     * Setter method for favIconUrl.
     *
     * @param aFavIconUrl the new value for favIconUrl
     */
    public void setFavIconUrl(String aFavIconUrl) {
        favIconUrl = aFavIconUrl;
    }

    /**
     * Access method for logoUrl.
     *
     * @return the current value of logoUrl
     */
    public String getLogoUrl() {
        return logoUrl;
    }

    /**
     * Setter method for logoUrl.
     *
     * @param aLogoUrl the new value for logoUrl
     */
    public void setLogoUrl(String aLogoUrl) {
        logoUrl = aLogoUrl;
    }

    /**
     * Access method for createdBy.
     *
     * @return the current value of createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter method for createdBy.
     *
     * @param aCreatedBy the new value for createdBy
     */
    public void setCreatedBy(String aCreatedBy) {
        createdBy = aCreatedBy;
    }

    /**
     * Access method for createdAt.
     *
     * @return the current value of createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * Setter method for createdAt.
     *
     * @param aCreatedAt the new value for createdAt
     */
    public void setCreatedAt(LocalDateTime aCreatedAt) {
        createdAt = aCreatedAt;
    }

    /**
     * Access method for updatedBy.
     *
     * @return the current value of updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Setter method for updatedBy.
     *
     * @param aUpdatedBy the new value for updatedBy
     */
    public void setUpdatedBy(String aUpdatedBy) {
        updatedBy = aUpdatedBy;
    }

    /**
     * Access method for updatedAt.
     *
     * @return the current value of updatedAt
     */
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Setter method for updatedAt.
     *
     * @param aUpdatedAt the new value for updatedAt
     */
    public void setUpdatedAt(LocalDateTime aUpdatedAt) {
        updatedAt = aUpdatedAt;
    }

    /**
     * Compares the key for this instance with another MerchantUrl.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MerchantUrl and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MerchantUrl)) {
            return false;
        }
        MerchantUrl that = (MerchantUrl) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MerchantUrl.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MerchantUrl)) return false;
        return this.equalKeys(other) && ((MerchantUrl)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MerchantUrl |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
