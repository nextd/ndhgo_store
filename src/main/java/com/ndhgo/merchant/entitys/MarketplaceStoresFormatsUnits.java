// Generated with g9.

package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="marketplace_stores_formats_units")
public class MarketplaceStoresFormatsUnits implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 936725804119354300L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="store_id", precision=10)
    private int storeId;
    @Column(length=3)
    private String currency;
    @Column(name="currency_prefix", length=1)
    private String currencyPrefix;
    @Column(name="currency_suffix", length=1)
    private String currencySuffix;
    @Column(name="currency_precision", precision=5)
    private short currencyPrecision;
    @Column(name="currency_group_separator", length=1)
    private String currencyGroupSeparator;
    @Column(name="currency_decimal_separator", length=1)
    private String currencyDecimalSeparator;
    @Column(name="currency_truncate_zero_fractional", precision=3)
    private short currencyTruncateZeroFractional;
    @Column(name="currency_rate", precision=10)
    private int currencyRate;
    @Column(name="weight_unit", length=3)
    private String weightUnit;
    @Column(name="weight_group_separator", length=1)
    private String weightGroupSeparator;
    @Column(name="weight_decimal_separator", length=1)
    private String weightDecimalSeparator;
    @Column(name="weight_truncate_zero_fractional", precision=3)
    private short weightTruncateZeroFractional;
    @Column(name="time_format", length=20)
    private String timeFormat;
    @Column(name="date_format", length=20)
    private String dateFormat;
    @Column(length=100)
    private String timezone;
    @Column(name="dimensions_unit", length=3)
    private String dimensionsUnit;
    @Column(name="order_number_prefix", length=10)
    private String orderNumberPrefix;
    @Column(name="order_number_suffix", length=10)
    private String orderNumberSuffix;

    /** Default constructor. */
    public MarketplaceStoresFormatsUnits() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for storeId.
     *
     * @return the current value of storeId
     */
    public int getStoreId() {
        return storeId;
    }

    /**
     * Setter method for storeId.
     *
     * @param aStoreId the new value for storeId
     */
    public void setStoreId(int aStoreId) {
        storeId = aStoreId;
    }

    /**
     * Access method for currency.
     *
     * @return the current value of currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Setter method for currency.
     *
     * @param aCurrency the new value for currency
     */
    public void setCurrency(String aCurrency) {
        currency = aCurrency;
    }

    /**
     * Access method for currencyPrefix.
     *
     * @return the current value of currencyPrefix
     */
    public String getCurrencyPrefix() {
        return currencyPrefix;
    }

    /**
     * Setter method for currencyPrefix.
     *
     * @param aCurrencyPrefix the new value for currencyPrefix
     */
    public void setCurrencyPrefix(String aCurrencyPrefix) {
        currencyPrefix = aCurrencyPrefix;
    }

    /**
     * Access method for currencySuffix.
     *
     * @return the current value of currencySuffix
     */
    public String getCurrencySuffix() {
        return currencySuffix;
    }

    /**
     * Setter method for currencySuffix.
     *
     * @param aCurrencySuffix the new value for currencySuffix
     */
    public void setCurrencySuffix(String aCurrencySuffix) {
        currencySuffix = aCurrencySuffix;
    }

    /**
     * Access method for currencyPrecision.
     *
     * @return the current value of currencyPrecision
     */
    public short getCurrencyPrecision() {
        return currencyPrecision;
    }

    /**
     * Setter method for currencyPrecision.
     *
     * @param aCurrencyPrecision the new value for currencyPrecision
     */
    public void setCurrencyPrecision(short aCurrencyPrecision) {
        currencyPrecision = aCurrencyPrecision;
    }

    /**
     * Access method for currencyGroupSeparator.
     *
     * @return the current value of currencyGroupSeparator
     */
    public String getCurrencyGroupSeparator() {
        return currencyGroupSeparator;
    }

    /**
     * Setter method for currencyGroupSeparator.
     *
     * @param aCurrencyGroupSeparator the new value for currencyGroupSeparator
     */
    public void setCurrencyGroupSeparator(String aCurrencyGroupSeparator) {
        currencyGroupSeparator = aCurrencyGroupSeparator;
    }

    /**
     * Access method for currencyDecimalSeparator.
     *
     * @return the current value of currencyDecimalSeparator
     */
    public String getCurrencyDecimalSeparator() {
        return currencyDecimalSeparator;
    }

    /**
     * Setter method for currencyDecimalSeparator.
     *
     * @param aCurrencyDecimalSeparator the new value for currencyDecimalSeparator
     */
    public void setCurrencyDecimalSeparator(String aCurrencyDecimalSeparator) {
        currencyDecimalSeparator = aCurrencyDecimalSeparator;
    }

    /**
     * Access method for currencyTruncateZeroFractional.
     *
     * @return the current value of currencyTruncateZeroFractional
     */
    public short getCurrencyTruncateZeroFractional() {
        return currencyTruncateZeroFractional;
    }

    /**
     * Setter method for currencyTruncateZeroFractional.
     *
     * @param aCurrencyTruncateZeroFractional the new value for currencyTruncateZeroFractional
     */
    public void setCurrencyTruncateZeroFractional(short aCurrencyTruncateZeroFractional) {
        currencyTruncateZeroFractional = aCurrencyTruncateZeroFractional;
    }

    /**
     * Access method for currencyRate.
     *
     * @return the current value of currencyRate
     */
    public int getCurrencyRate() {
        return currencyRate;
    }

    /**
     * Setter method for currencyRate.
     *
     * @param aCurrencyRate the new value for currencyRate
     */
    public void setCurrencyRate(int aCurrencyRate) {
        currencyRate = aCurrencyRate;
    }

    /**
     * Access method for weightUnit.
     *
     * @return the current value of weightUnit
     */
    public String getWeightUnit() {
        return weightUnit;
    }

    /**
     * Setter method for weightUnit.
     *
     * @param aWeightUnit the new value for weightUnit
     */
    public void setWeightUnit(String aWeightUnit) {
        weightUnit = aWeightUnit;
    }

    /**
     * Access method for weightGroupSeparator.
     *
     * @return the current value of weightGroupSeparator
     */
    public String getWeightGroupSeparator() {
        return weightGroupSeparator;
    }

    /**
     * Setter method for weightGroupSeparator.
     *
     * @param aWeightGroupSeparator the new value for weightGroupSeparator
     */
    public void setWeightGroupSeparator(String aWeightGroupSeparator) {
        weightGroupSeparator = aWeightGroupSeparator;
    }

    /**
     * Access method for weightDecimalSeparator.
     *
     * @return the current value of weightDecimalSeparator
     */
    public String getWeightDecimalSeparator() {
        return weightDecimalSeparator;
    }

    /**
     * Setter method for weightDecimalSeparator.
     *
     * @param aWeightDecimalSeparator the new value for weightDecimalSeparator
     */
    public void setWeightDecimalSeparator(String aWeightDecimalSeparator) {
        weightDecimalSeparator = aWeightDecimalSeparator;
    }

    /**
     * Access method for weightTruncateZeroFractional.
     *
     * @return the current value of weightTruncateZeroFractional
     */
    public short getWeightTruncateZeroFractional() {
        return weightTruncateZeroFractional;
    }

    /**
     * Setter method for weightTruncateZeroFractional.
     *
     * @param aWeightTruncateZeroFractional the new value for weightTruncateZeroFractional
     */
    public void setWeightTruncateZeroFractional(short aWeightTruncateZeroFractional) {
        weightTruncateZeroFractional = aWeightTruncateZeroFractional;
    }

    /**
     * Access method for timeFormat.
     *
     * @return the current value of timeFormat
     */
    public String getTimeFormat() {
        return timeFormat;
    }

    /**
     * Setter method for timeFormat.
     *
     * @param aTimeFormat the new value for timeFormat
     */
    public void setTimeFormat(String aTimeFormat) {
        timeFormat = aTimeFormat;
    }

    /**
     * Access method for dateFormat.
     *
     * @return the current value of dateFormat
     */
    public String getDateFormat() {
        return dateFormat;
    }

    /**
     * Setter method for dateFormat.
     *
     * @param aDateFormat the new value for dateFormat
     */
    public void setDateFormat(String aDateFormat) {
        dateFormat = aDateFormat;
    }

    /**
     * Access method for timezone.
     *
     * @return the current value of timezone
     */
    public String getTimezone() {
        return timezone;
    }

    /**
     * Setter method for timezone.
     *
     * @param aTimezone the new value for timezone
     */
    public void setTimezone(String aTimezone) {
        timezone = aTimezone;
    }

    /**
     * Access method for dimensionsUnit.
     *
     * @return the current value of dimensionsUnit
     */
    public String getDimensionsUnit() {
        return dimensionsUnit;
    }

    /**
     * Setter method for dimensionsUnit.
     *
     * @param aDimensionsUnit the new value for dimensionsUnit
     */
    public void setDimensionsUnit(String aDimensionsUnit) {
        dimensionsUnit = aDimensionsUnit;
    }

    /**
     * Access method for orderNumberPrefix.
     *
     * @return the current value of orderNumberPrefix
     */
    public String getOrderNumberPrefix() {
        return orderNumberPrefix;
    }

    /**
     * Setter method for orderNumberPrefix.
     *
     * @param aOrderNumberPrefix the new value for orderNumberPrefix
     */
    public void setOrderNumberPrefix(String aOrderNumberPrefix) {
        orderNumberPrefix = aOrderNumberPrefix;
    }

    /**
     * Access method for orderNumberSuffix.
     *
     * @return the current value of orderNumberSuffix
     */
    public String getOrderNumberSuffix() {
        return orderNumberSuffix;
    }

    /**
     * Setter method for orderNumberSuffix.
     *
     * @param aOrderNumberSuffix the new value for orderNumberSuffix
     */
    public void setOrderNumberSuffix(String aOrderNumberSuffix) {
        orderNumberSuffix = aOrderNumberSuffix;
    }

    /**
     * Compares the key for this instance with another MarketplaceStoresFormatsUnits.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MarketplaceStoresFormatsUnits and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MarketplaceStoresFormatsUnits)) {
            return false;
        }
        MarketplaceStoresFormatsUnits that = (MarketplaceStoresFormatsUnits) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MarketplaceStoresFormatsUnits.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MarketplaceStoresFormatsUnits)) return false;
        return this.equalKeys(other) && ((MarketplaceStoresFormatsUnits)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MarketplaceStoresFormatsUnits |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
