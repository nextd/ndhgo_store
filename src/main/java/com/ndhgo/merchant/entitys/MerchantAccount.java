/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;




/**
 * @author Akash Chakraborty
 *
 */
@Entity(name="merchant_account")
public class MerchantAccount implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6107495615586890488L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="account_name", length=200)
    private String accountName;
    @Column(length=200)
    private String email;
    @Column(length=10)
    private String mobile;
    @Column(name="primary_country", length=100)
    private String primaryCountry;
    @Column(name="primary_state", length=100)
    private String primaryState;
    @Column(name="primary_city", length=200)
    private String primaryCity;
    @Column(name="business_category_id", nullable=false, precision=10)
    private int businessCategoryId;
    @Column(name="business_category", length=150)
    private String businessCategory;
    @Column(name="is_active", length=3)
    private boolean isActive;
    @Column(name="subscription_ref_id", precision=10)
    private int subscriptionRefId;
    @Column(name="subscription_level", length=12)
    private String subscriptionLevel;
    @Column(name="subscription_start_date")
    private LocalDateTime subscriptionStartDate;
    @Column(name="subscription_expiry_on")
    private LocalDateTime subscriptionExpiryOn;
    @Column(name="max_stores_allowed", precision=10)
    private int maxStoresAllowed;
    @Column(length=9)
    private String status;
    @Column(name="created_by", length=50)
    private String createdBy;
    @Column(name="created_at")
    private LocalDateTime createdAt;
    @Column(name="updated_by", length=50)
    private String updatedBy;
    @Column(name="updated_at")
    private LocalDateTime updatedAt;
    @OneToMany(mappedBy="merchantAccount")
    private Set<MerchantLocation> merchantLocation;

    /** Default constructor. */
    public MerchantAccount() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for accountName.
     *
     * @return the current value of accountName
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Setter method for accountName.
     *
     * @param aAccountName the new value for accountName
     */
    public void setAccountName(String aAccountName) {
        accountName = aAccountName;
    }

    /**
     * Access method for email.
     *
     * @return the current value of email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method for email.
     *
     * @param aEmail the new value for email
     */
    public void setEmail(String aEmail) {
        email = aEmail;
    }

    /**
     * Access method for mobile.
     *
     * @return the current value of mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Setter method for mobile.
     *
     * @param aMobile the new value for mobile
     */
    public void setMobile(String aMobile) {
        mobile = aMobile;
    }

    /**
     * Access method for primaryCountry.
     *
     * @return the current value of primaryCountry
     */
    public String getPrimaryCountry() {
        return primaryCountry;
    }

    /**
     * Setter method for primaryCountry.
     *
     * @param aPrimaryCountry the new value for primaryCountry
     */
    public void setPrimaryCountry(String aPrimaryCountry) {
        primaryCountry = aPrimaryCountry;
    }

    /**
     * Access method for primaryState.
     *
     * @return the current value of primaryState
     */
    public String getPrimaryState() {
        return primaryState;
    }

    /**
     * Setter method for primaryState.
     *
     * @param aPrimaryState the new value for primaryState
     */
    public void setPrimaryState(String aPrimaryState) {
        primaryState = aPrimaryState;
    }

    /**
     * Access method for primaryCity.
     *
     * @return the current value of primaryCity
     */
    public String getPrimaryCity() {
        return primaryCity;
    }

    /**
     * Setter method for primaryCity.
     *
     * @param aPrimaryCity the new value for primaryCity
     */
    public void setPrimaryCity(String aPrimaryCity) {
        primaryCity = aPrimaryCity;
    }

    /**
     * Access method for businessCategoryId.
     *
     * @return the current value of businessCategoryId
     */
    public int getBusinessCategoryId() {
        return businessCategoryId;
    }

    /**
     * Setter method for businessCategoryId.
     *
     * @param aBusinessCategoryId the new value for businessCategoryId
     */
    public void setBusinessCategoryId(int aBusinessCategoryId) {
        businessCategoryId = aBusinessCategoryId;
    }

    /**
     * Access method for businessCategory.
     *
     * @return the current value of businessCategory
     */
    public String getBusinessCategory() {
        return businessCategory;
    }

    /**
     * Setter method for businessCategory.
     *
     * @param aBusinessCategory the new value for businessCategory
     */
    public void setBusinessCategory(String aBusinessCategory) {
        businessCategory = aBusinessCategory;
    }

    /**
     * Access method for isActive.
     *
     * @return true if and only if isActive is currently true
     */
    public boolean getIsActive() {
        return isActive;
    }

    /**
     * Setter method for isActive.
     *
     * @param aIsActive the new value for isActive
     */
    public void setIsActive(boolean aIsActive) {
        isActive = aIsActive;
    }

    /**
     * Access method for subscriptionRefId.
     *
     * @return the current value of subscriptionRefId
     */
    public int getSubscriptionRefId() {
        return subscriptionRefId;
    }

    /**
     * Setter method for subscriptionRefId.
     *
     * @param aSubscriptionRefId the new value for subscriptionRefId
     */
    public void setSubscriptionRefId(int aSubscriptionRefId) {
        subscriptionRefId = aSubscriptionRefId;
    }

    /**
     * Access method for subscriptionLevel.
     *
     * @return the current value of subscriptionLevel
     */
    public String getSubscriptionLevel() {
        return subscriptionLevel;
    }

    /**
     * Setter method for subscriptionLevel.
     *
     * @param aSubscriptionLevel the new value for subscriptionLevel
     */
    public void setSubscriptionLevel(String aSubscriptionLevel) {
        subscriptionLevel = aSubscriptionLevel;
    }

    /**
     * Access method for subscriptionStartDate.
     *
     * @return the current value of subscriptionStartDate
     */
    public LocalDateTime getSubscriptionStartDate() {
        return subscriptionStartDate;
    }

    /**
     * Setter method for subscriptionStartDate.
     *
     * @param aSubscriptionStartDate the new value for subscriptionStartDate
     */
    public void setSubscriptionStartDate(LocalDateTime aSubscriptionStartDate) {
        subscriptionStartDate = aSubscriptionStartDate;
    }

    /**
     * Access method for subscriptionExpiryOn.
     *
     * @return the current value of subscriptionExpiryOn
     */
    public LocalDateTime getSubscriptionExpiryOn() {
        return subscriptionExpiryOn;
    }

    /**
     * Setter method for subscriptionExpiryOn.
     *
     * @param aSubscriptionExpiryOn the new value for subscriptionExpiryOn
     */
    public void setSubscriptionExpiryOn(LocalDateTime aSubscriptionExpiryOn) {
        subscriptionExpiryOn = aSubscriptionExpiryOn;
    }

    /**
     * Access method for maxStoresAllowed.
     *
     * @return the current value of maxStoresAllowed
     */
    public int getMaxStoresAllowed() {
        return maxStoresAllowed;
    }

    /**
     * Setter method for maxStoresAllowed.
     *
     * @param aMaxStoresAllowed the new value for maxStoresAllowed
     */
    public void setMaxStoresAllowed(int aMaxStoresAllowed) {
        maxStoresAllowed = aMaxStoresAllowed;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(String aStatus) {
        status = aStatus;
    }

    /**
     * Access method for createdBy.
     *
     * @return the current value of createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter method for createdBy.
     *
     * @param aCreatedBy the new value for createdBy
     */
    public void setCreatedBy(String aCreatedBy) {
        createdBy = aCreatedBy;
    }

    /**
     * Access method for createdAt.
     *
     * @return the current value of createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * Setter method for createdAt.
     *
     * @param aCreatedAt the new value for createdAt
     */
    public void setCreatedAt(LocalDateTime aCreatedAt) {
        createdAt = aCreatedAt;
    }

    /**
     * Access method for updatedBy.
     *
     * @return the current value of updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Setter method for updatedBy.
     *
     * @param aUpdatedBy the new value for updatedBy
     */
    public void setUpdatedBy(String aUpdatedBy) {
        updatedBy = aUpdatedBy;
    }

    /**
     * Access method for updatedAt.
     *
     * @return the current value of updatedAt
     */
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Setter method for updatedAt.
     *
     * @param aUpdatedAt the new value for updatedAt
     */
    public void setUpdatedAt(LocalDateTime aUpdatedAt) {
        updatedAt = aUpdatedAt;
    }

    /**
     * Access method for merchantLocation.
     *
     * @return the current value of merchantLocation
     */
    public Set<MerchantLocation> getMerchantLocation() {
        return merchantLocation;
    }

    /**
     * Setter method for merchantLocation.
     *
     * @param aMerchantLocation the new value for merchantLocation
     */
    public void setMerchantLocation(Set<MerchantLocation> aMerchantLocation) {
        merchantLocation = aMerchantLocation;
    }

    /**
     * Compares the key for this instance with another MerchantAccount.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MerchantAccount and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MerchantAccount)) {
            return false;
        }
        MerchantAccount that = (MerchantAccount) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MerchantAccount.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MerchantAccount)) return false;
        return this.equalKeys(other) && ((MerchantAccount)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MerchantAccount |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
