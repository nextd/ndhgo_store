/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;



/**
 * @author Akash Chakraborty
 *
 */
@Entity(name="merchant_kyc_docs")
public class MerchantKycDocs implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7310870272954713390L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="bank_account_id", precision=10)
    private int bankAccountId;
    @Column(name="doc_type", length=16)
    private String docType;
    @Column(name="doc_path")
    private String docPath;
    @Column(name="created_at", length=45)
    private String createdAt;
    @Column(name="updated_at", length=45)
    private String updatedAt;

    /** Default constructor. */
    public MerchantKycDocs() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for bankAccountId.
     *
     * @return the current value of bankAccountId
     */
    public int getBankAccountId() {
        return bankAccountId;
    }

    /**
     * Setter method for bankAccountId.
     *
     * @param aBankAccountId the new value for bankAccountId
     */
    public void setBankAccountId(int aBankAccountId) {
        bankAccountId = aBankAccountId;
    }

    /**
     * Access method for docType.
     *
     * @return the current value of docType
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Setter method for docType.
     *
     * @param aDocType the new value for docType
     */
    public void setDocType(String aDocType) {
        docType = aDocType;
    }

    /**
     * Access method for docPath.
     *
     * @return the current value of docPath
     */
    public String getDocPath() {
        return docPath;
    }

    /**
     * Setter method for docPath.
     *
     * @param aDocPath the new value for docPath
     */
    public void setDocPath(String aDocPath) {
        docPath = aDocPath;
    }

    /**
     * Access method for createdAt.
     *
     * @return the current value of createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * Setter method for createdAt.
     *
     * @param aCreatedAt the new value for createdAt
     */
    public void setCreatedAt(String aCreatedAt) {
        createdAt = aCreatedAt;
    }

    /**
     * Access method for updatedAt.
     *
     * @return the current value of updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Setter method for updatedAt.
     *
     * @param aUpdatedAt the new value for updatedAt
     */
    public void setUpdatedAt(String aUpdatedAt) {
        updatedAt = aUpdatedAt;
    }

    /**
     * Compares the key for this instance with another MerchantKycDocs.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MerchantKycDocs and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MerchantKycDocs)) {
            return false;
        }
        MerchantKycDocs that = (MerchantKycDocs) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MerchantKycDocs.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MerchantKycDocs)) return false;
        return this.equalKeys(other) && ((MerchantKycDocs)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MerchantKycDocs |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
