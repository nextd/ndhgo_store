/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;



/**
 * @author Akash Chakraborty
 *
 */
@Entity(name="marketplace_stores_pos")
public class MarketplaceStoresPos implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 764232609667834699L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="store_id", precision=10)
    private int storeId;
    @Column(name="pos_id", precision=10)
    private int posId;
    @Column(name="pos_store_id", length=45)
    private String posStoreId;
    @Column(name="pos_channel", length=45)
    private String posChannel;
    private String settings;
    @Column(name="created_by", length=45)
    private String createdBy;
    @Column(name="created_at")
    private LocalDateTime createdAt;
    @Column(name="updated_by", length=45)
    private String updatedBy;
    @Column(name="updated_at")
    private LocalDateTime updatedAt;
    @Column(name="user_id", precision=10)
    private int userId;
    @Column(name="auth_key")
    private String authKey;
    @Column(name="valid_upto")
    private LocalDateTime validUpto;

    /** Default constructor. */
    public MarketplaceStoresPos() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for storeId.
     *
     * @return the current value of storeId
     */
    public int getStoreId() {
        return storeId;
    }

    /**
     * Setter method for storeId.
     *
     * @param aStoreId the new value for storeId
     */
    public void setStoreId(int aStoreId) {
        storeId = aStoreId;
    }

    /**
     * Access method for posId.
     *
     * @return the current value of posId
     */
    public int getPosId() {
        return posId;
    }

    /**
     * Setter method for posId.
     *
     * @param aPosId the new value for posId
     */
    public void setPosId(int aPosId) {
        posId = aPosId;
    }

    /**
     * Access method for posStoreId.
     *
     * @return the current value of posStoreId
     */
    public String getPosStoreId() {
        return posStoreId;
    }

    /**
     * Setter method for posStoreId.
     *
     * @param aPosStoreId the new value for posStoreId
     */
    public void setPosStoreId(String aPosStoreId) {
        posStoreId = aPosStoreId;
    }

    /**
     * Access method for posChannel.
     *
     * @return the current value of posChannel
     */
    public String getPosChannel() {
        return posChannel;
    }

    /**
     * Setter method for posChannel.
     *
     * @param aPosChannel the new value for posChannel
     */
    public void setPosChannel(String aPosChannel) {
        posChannel = aPosChannel;
    }

    /**
     * Access method for settings.
     *
     * @return the current value of settings
     */
    public String getSettings() {
        return settings;
    }

    /**
     * Setter method for settings.
     *
     * @param aSettings the new value for settings
     */
    public void setSettings(String aSettings) {
        settings = aSettings;
    }

    /**
     * Access method for createdBy.
     *
     * @return the current value of createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter method for createdBy.
     *
     * @param aCreatedBy the new value for createdBy
     */
    public void setCreatedBy(String aCreatedBy) {
        createdBy = aCreatedBy;
    }

    /**
     * Access method for createdAt.
     *
     * @return the current value of createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * Setter method for createdAt.
     *
     * @param aCreatedAt the new value for createdAt
     */
    public void setCreatedAt(LocalDateTime aCreatedAt) {
        createdAt = aCreatedAt;
    }

    /**
     * Access method for updatedBy.
     *
     * @return the current value of updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Setter method for updatedBy.
     *
     * @param aUpdatedBy the new value for updatedBy
     */
    public void setUpdatedBy(String aUpdatedBy) {
        updatedBy = aUpdatedBy;
    }

    /**
     * Access method for updatedAt.
     *
     * @return the current value of updatedAt
     */
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Setter method for updatedAt.
     *
     * @param aUpdatedAt the new value for updatedAt
     */
    public void setUpdatedAt(LocalDateTime aUpdatedAt) {
        updatedAt = aUpdatedAt;
    }

    /**
     * Access method for userId.
     *
     * @return the current value of userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Setter method for userId.
     *
     * @param aUserId the new value for userId
     */
    public void setUserId(int aUserId) {
        userId = aUserId;
    }

    /**
     * Access method for authKey.
     *
     * @return the current value of authKey
     */
    public String getAuthKey() {
        return authKey;
    }

    /**
     * Setter method for authKey.
     *
     * @param aAuthKey the new value for authKey
     */
    public void setAuthKey(String aAuthKey) {
        authKey = aAuthKey;
    }

    /**
     * Access method for validUpto.
     *
     * @return the current value of validUpto
     */
    public LocalDateTime getValidUpto() {
        return validUpto;
    }

    /**
     * Setter method for validUpto.
     *
     * @param aValidUpto the new value for validUpto
     */
    public void setValidUpto(LocalDateTime aValidUpto) {
        validUpto = aValidUpto;
    }

    /**
     * Compares the key for this instance with another MarketplaceStoresPos.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MarketplaceStoresPos and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MarketplaceStoresPos)) {
            return false;
        }
        MarketplaceStoresPos that = (MarketplaceStoresPos) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MarketplaceStoresPos.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MarketplaceStoresPos)) return false;
        return this.equalKeys(other) && ((MarketplaceStoresPos)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MarketplaceStoresPos |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
