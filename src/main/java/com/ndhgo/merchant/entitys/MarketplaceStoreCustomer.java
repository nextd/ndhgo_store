/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * @author Akash Chakraborty
 *
 */
@Entity
@Table(name="marketplace_store_customer", indexes={@Index(name="marketplace_store_customer_uniqueIndex_IX", columnList="uniqueIndex", unique=true)})
public class MarketplaceStoreCustomer implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2620334860051882639L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name="storeId", column=@Column(name="uniqueIndex_store_id")),
        @AttributeOverride(name="userId", column=@Column(name="uniqueIndex_user_id"))
    })
    @Column(unique=true)
    private UniqueIndex uniqueIndex;
    @Column(name="user_mobile", length=10)
    private String userMobile;
    @Column(name="user_email", length=200)
    private String userEmail;
    @Column(length=3)
    private boolean status;
    @Column(name="request_date")
    private LocalDateTime requestDate;
    @Column(name="order_count", precision=10)
    private int orderCount;
    @Column(name="order_value", precision=22)
    private double orderValue;
    @Column(name="last_order_date")
    private LocalDateTime lastOrderDate;
    @Column(name="seller_address_sync", length=3)
    private boolean sellerAddressSync;
    @Column(name="joining_status", precision=3)
    private short joiningStatus;
    @Column(name="store_search", length=3)
    private boolean storeSearch;
    @Column(name="customer_favorite", length=3)
    private boolean customerFavorite;
    @Column(name="accepted_date")
    private LocalDateTime acceptedDate;
    @Column(name="joining_by_request", length=3)
    private boolean joiningByRequest;
    @Column(name="request_url")
    private String requestUrl;
    @Column(name="contact_name", length=200)
    private String contactName;
    @Column(name="contact_img_url")
    private String contactImgUrl;
    private String address;
    @Column(name="g_lat", precision=22)
    private double gLat;
    @Column(name="g_long", precision=22)
    private double gLong;
    @Column(name="elastic_index", length=45)
    private String elasticIndex;

    /** Default constructor. */
    public MarketplaceStoreCustomer() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for uniqueIndex.
     *
     * @return the current value of uniqueIndex
     */
    public UniqueIndex getUniqueIndex() {
        return uniqueIndex;
    }

    /**
     * Setter method for uniqueIndex.
     *
     * @param aUniqueIndex the new value for uniqueIndex
     */
    public void setUniqueIndex(UniqueIndex aUniqueIndex) {
        uniqueIndex = aUniqueIndex;
    }

    /**
     * Access method for userMobile.
     *
     * @return the current value of userMobile
     */
    public String getUserMobile() {
        return userMobile;
    }

    /**
     * Setter method for userMobile.
     *
     * @param aUserMobile the new value for userMobile
     */
    public void setUserMobile(String aUserMobile) {
        userMobile = aUserMobile;
    }

    /**
     * Access method for userEmail.
     *
     * @return the current value of userEmail
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Setter method for userEmail.
     *
     * @param aUserEmail the new value for userEmail
     */
    public void setUserEmail(String aUserEmail) {
        userEmail = aUserEmail;
    }

    /**
     * Access method for status.
     *
     * @return true if and only if status is currently true
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(boolean aStatus) {
        status = aStatus;
    }

    /**
     * Access method for requestDate.
     *
     * @return the current value of requestDate
     */
    public LocalDateTime getRequestDate() {
        return requestDate;
    }

    /**
     * Setter method for requestDate.
     *
     * @param aRequestDate the new value for requestDate
     */
    public void setRequestDate(LocalDateTime aRequestDate) {
        requestDate = aRequestDate;
    }

    /**
     * Access method for orderCount.
     *
     * @return the current value of orderCount
     */
    public int getOrderCount() {
        return orderCount;
    }

    /**
     * Setter method for orderCount.
     *
     * @param aOrderCount the new value for orderCount
     */
    public void setOrderCount(int aOrderCount) {
        orderCount = aOrderCount;
    }

    /**
     * Access method for orderValue.
     *
     * @return the current value of orderValue
     */
    public double getOrderValue() {
        return orderValue;
    }

    /**
     * Setter method for orderValue.
     *
     * @param aOrderValue the new value for orderValue
     */
    public void setOrderValue(double aOrderValue) {
        orderValue = aOrderValue;
    }

    /**
     * Access method for lastOrderDate.
     *
     * @return the current value of lastOrderDate
     */
    public LocalDateTime getLastOrderDate() {
        return lastOrderDate;
    }

    /**
     * Setter method for lastOrderDate.
     *
     * @param aLastOrderDate the new value for lastOrderDate
     */
    public void setLastOrderDate(LocalDateTime aLastOrderDate) {
        lastOrderDate = aLastOrderDate;
    }

    /**
     * Access method for sellerAddressSync.
     *
     * @return true if and only if sellerAddressSync is currently true
     */
    public boolean getSellerAddressSync() {
        return sellerAddressSync;
    }

    /**
     * Setter method for sellerAddressSync.
     *
     * @param aSellerAddressSync the new value for sellerAddressSync
     */
    public void setSellerAddressSync(boolean aSellerAddressSync) {
        sellerAddressSync = aSellerAddressSync;
    }

    /**
     * Access method for joiningStatus.
     *
     * @return the current value of joiningStatus
     */
    public short getJoiningStatus() {
        return joiningStatus;
    }

    /**
     * Setter method for joiningStatus.
     *
     * @param aJoiningStatus the new value for joiningStatus
     */
    public void setJoiningStatus(short aJoiningStatus) {
        joiningStatus = aJoiningStatus;
    }

    /**
     * Access method for storeSearch.
     *
     * @return true if and only if storeSearch is currently true
     */
    public boolean getStoreSearch() {
        return storeSearch;
    }

    /**
     * Setter method for storeSearch.
     *
     * @param aStoreSearch the new value for storeSearch
     */
    public void setStoreSearch(boolean aStoreSearch) {
        storeSearch = aStoreSearch;
    }

    /**
     * Access method for customerFavorite.
     *
     * @return true if and only if customerFavorite is currently true
     */
    public boolean getCustomerFavorite() {
        return customerFavorite;
    }

    /**
     * Setter method for customerFavorite.
     *
     * @param aCustomerFavorite the new value for customerFavorite
     */
    public void setCustomerFavorite(boolean aCustomerFavorite) {
        customerFavorite = aCustomerFavorite;
    }

    /**
     * Access method for acceptedDate.
     *
     * @return the current value of acceptedDate
     */
    public LocalDateTime getAcceptedDate() {
        return acceptedDate;
    }

    /**
     * Setter method for acceptedDate.
     *
     * @param aAcceptedDate the new value for acceptedDate
     */
    public void setAcceptedDate(LocalDateTime aAcceptedDate) {
        acceptedDate = aAcceptedDate;
    }

    /**
     * Access method for joiningByRequest.
     *
     * @return true if and only if joiningByRequest is currently true
     */
    public boolean getJoiningByRequest() {
        return joiningByRequest;
    }

    /**
     * Setter method for joiningByRequest.
     *
     * @param aJoiningByRequest the new value for joiningByRequest
     */
    public void setJoiningByRequest(boolean aJoiningByRequest) {
        joiningByRequest = aJoiningByRequest;
    }

    /**
     * Access method for requestUrl.
     *
     * @return the current value of requestUrl
     */
    public String getRequestUrl() {
        return requestUrl;
    }

    /**
     * Setter method for requestUrl.
     *
     * @param aRequestUrl the new value for requestUrl
     */
    public void setRequestUrl(String aRequestUrl) {
        requestUrl = aRequestUrl;
    }

    /**
     * Access method for contactName.
     *
     * @return the current value of contactName
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * Setter method for contactName.
     *
     * @param aContactName the new value for contactName
     */
    public void setContactName(String aContactName) {
        contactName = aContactName;
    }

    /**
     * Access method for contactImgUrl.
     *
     * @return the current value of contactImgUrl
     */
    public String getContactImgUrl() {
        return contactImgUrl;
    }

    /**
     * Setter method for contactImgUrl.
     *
     * @param aContactImgUrl the new value for contactImgUrl
     */
    public void setContactImgUrl(String aContactImgUrl) {
        contactImgUrl = aContactImgUrl;
    }

    /**
     * Access method for address.
     *
     * @return the current value of address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setter method for address.
     *
     * @param aAddress the new value for address
     */
    public void setAddress(String aAddress) {
        address = aAddress;
    }

    /**
     * Access method for gLat.
     *
     * @return the current value of gLat
     */
    public double getGLat() {
        return gLat;
    }

    /**
     * Setter method for gLat.
     *
     * @param aGLat the new value for gLat
     */
    public void setGLat(double aGLat) {
        gLat = aGLat;
    }

    /**
     * Access method for gLong.
     *
     * @return the current value of gLong
     */
    public double getGLong() {
        return gLong;
    }

    /**
     * Setter method for gLong.
     *
     * @param aGLong the new value for gLong
     */
    public void setGLong(double aGLong) {
        gLong = aGLong;
    }

    /**
     * Access method for elasticIndex.
     *
     * @return the current value of elasticIndex
     */
    public String getElasticIndex() {
        return elasticIndex;
    }

    /**
     * Setter method for elasticIndex.
     *
     * @param aElasticIndex the new value for elasticIndex
     */
    public void setElasticIndex(String aElasticIndex) {
        elasticIndex = aElasticIndex;
    }

    /**
     * Compares the key for this instance with another MarketplaceStoreCustomer.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MarketplaceStoreCustomer and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MarketplaceStoreCustomer)) {
            return false;
        }
        MarketplaceStoreCustomer that = (MarketplaceStoreCustomer) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MarketplaceStoreCustomer.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MarketplaceStoreCustomer)) return false;
        return this.equalKeys(other) && ((MarketplaceStoreCustomer)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MarketplaceStoreCustomer |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
