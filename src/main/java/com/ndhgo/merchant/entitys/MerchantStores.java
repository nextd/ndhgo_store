/**
 * Copyright (c) 2022, NDHGO Technologies All Rights Reserved.
 *
 */
package com.ndhgo.merchant.entitys;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;


/**
 * @author Akash Chakraborty
 *
 */
@Entity(name="merchant_stores")
public class MerchantStores implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1041416780497701839L;

	/** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="account_id", precision=10)
    private int accountId;
    @Column(name="location_id", precision=10)
    private int locationId;
    @Column(name="brand_id", precision=10)
    private int brandId;
    @Column(name="bank_account_id", precision=10)
    private int bankAccountId;
    @Column(name="account_ledger_code", length=30)
    private String accountLedgerCode;
    @Column(name="settlement_group_id", precision=10)
    private int settlementGroupId;
    @Column(name="business_category_id", precision=10)
    private int businessCategoryId;
    @Column(name="business_category_name", length=150)
    private String businessCategoryName;
    @Column(name="business_category_sector_id", precision=10)
    private int businessCategorySectorId;
    @Column(name="business_category_sector_name", length=150)
    private String businessCategorySectorName;
    @Column(name="store_name", length=50)
    private String storeName;
    @Column(length=3)
    private String currency;
    @Column(name="contact_number", length=20)
    private String contactNumber;
    @Column(name="local_listing_range", precision=10)
    private int localListingRange;
    @Column(name="delivery_attributes")
    private String deliveryAttributes;
    @Column(name="store_operation")
    private String storeOperation;
    @Column(name="business_hours_limitation_type", length=16)
    private String businessHoursLimitationType;
    @Column(name="location_keyword")
    private String locationKeyword;
    @Column(name="category_keyword")
    private String categoryKeyword;
    private String keywords;
    @Column(name="is_live", length=3)
    private boolean isLive;
    @Column(length=9)
    private String status;
    @Column(name="store_type", length=7)
    private String storeType;
    @Column(name="is_brand_store", length=3)
    private boolean isBrandStore;
    @Column(name="is_verified_store", length=3)
    private boolean isVerifiedStore;
    @Column(name="created_by", length=50)
    private String createdBy;
    @Column(name="created_at", nullable=false)
    private LocalDateTime createdAt;
    @Column(name="updated_at")
    private LocalDateTime updatedAt;
    @Column(name="updated_by", length=50)
    private String updatedBy;
    @Column(name="allow_marketplace", length=3)
    private boolean allowMarketplace;
    @Column(name="allow_local_listing", length=3)
    private boolean allowLocalListing;
    @Column(name="promotional_keywords")
    private String promotionalKeywords;
    @Column(name="zone_id", precision=10)
    private int zoneId;
    @Column(name="store_cover_image")
    private String storeCoverImage;
    @Column(name="category_ids")
    private String categoryIds;
    @Column(name="reject_user", length=100)
    private String rejectUser;
    @Column(name="reject_remarks", length=200)
    private String rejectRemarks;
    @Column(name="tag_line", length=200)
    private String tagLine;
    @Column(name="authorized_certificates")
    private String authorizedCertificates;
    @Column(name="order_processed", precision=10)
    private int orderProcessed;
    @Column(name="payment_options")
    private String paymentOptions;
    @Column(name="cancel_return_options")
    private String cancelReturnOptions;
    @Column(name="allow_scheduled_delivery", length=3)
    private boolean allowScheduledDelivery;
    @Column(name="scheduled_delivery_length_minute", precision=10)
    private int scheduledDeliveryLengthMinute;
    @Column(name="additional_packaging_charge", precision=10, scale=2)
    private double additionalPackagingCharge;
    @Column(name="packaging_charge_applicable", length=5)
    private String packagingChargeApplicable;
    @Column(name="tax_applicable", length=8)
    private String taxApplicable;
    @Column(name="tax_percentage", precision=10, scale=2)
    private double taxPercentage;
    @Column(name="custom_order", length=3)
    private boolean customOrder;
    @Column(name="pos_enabled", length=3)
    private boolean posEnabled;

    /** Default constructor. */
    public MerchantStores() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for accountId.
     *
     * @return the current value of accountId
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Setter method for accountId.
     *
     * @param aAccountId the new value for accountId
     */
    public void setAccountId(int aAccountId) {
        accountId = aAccountId;
    }

    /**
     * Access method for locationId.
     *
     * @return the current value of locationId
     */
    public int getLocationId() {
        return locationId;
    }

    /**
     * Setter method for locationId.
     *
     * @param aLocationId the new value for locationId
     */
    public void setLocationId(int aLocationId) {
        locationId = aLocationId;
    }

    /**
     * Access method for brandId.
     *
     * @return the current value of brandId
     */
    public int getBrandId() {
        return brandId;
    }

    /**
     * Setter method for brandId.
     *
     * @param aBrandId the new value for brandId
     */
    public void setBrandId(int aBrandId) {
        brandId = aBrandId;
    }

    /**
     * Access method for bankAccountId.
     *
     * @return the current value of bankAccountId
     */
    public int getBankAccountId() {
        return bankAccountId;
    }

    /**
     * Setter method for bankAccountId.
     *
     * @param aBankAccountId the new value for bankAccountId
     */
    public void setBankAccountId(int aBankAccountId) {
        bankAccountId = aBankAccountId;
    }

    /**
     * Access method for accountLedgerCode.
     *
     * @return the current value of accountLedgerCode
     */
    public String getAccountLedgerCode() {
        return accountLedgerCode;
    }

    /**
     * Setter method for accountLedgerCode.
     *
     * @param aAccountLedgerCode the new value for accountLedgerCode
     */
    public void setAccountLedgerCode(String aAccountLedgerCode) {
        accountLedgerCode = aAccountLedgerCode;
    }

    /**
     * Access method for settlementGroupId.
     *
     * @return the current value of settlementGroupId
     */
    public int getSettlementGroupId() {
        return settlementGroupId;
    }

    /**
     * Setter method for settlementGroupId.
     *
     * @param aSettlementGroupId the new value for settlementGroupId
     */
    public void setSettlementGroupId(int aSettlementGroupId) {
        settlementGroupId = aSettlementGroupId;
    }

    /**
     * Access method for businessCategoryId.
     *
     * @return the current value of businessCategoryId
     */
    public int getBusinessCategoryId() {
        return businessCategoryId;
    }

    /**
     * Setter method for businessCategoryId.
     *
     * @param aBusinessCategoryId the new value for businessCategoryId
     */
    public void setBusinessCategoryId(int aBusinessCategoryId) {
        businessCategoryId = aBusinessCategoryId;
    }

    /**
     * Access method for businessCategoryName.
     *
     * @return the current value of businessCategoryName
     */
    public String getBusinessCategoryName() {
        return businessCategoryName;
    }

    /**
     * Setter method for businessCategoryName.
     *
     * @param aBusinessCategoryName the new value for businessCategoryName
     */
    public void setBusinessCategoryName(String aBusinessCategoryName) {
        businessCategoryName = aBusinessCategoryName;
    }

    /**
     * Access method for businessCategorySectorId.
     *
     * @return the current value of businessCategorySectorId
     */
    public int getBusinessCategorySectorId() {
        return businessCategorySectorId;
    }

    /**
     * Setter method for businessCategorySectorId.
     *
     * @param aBusinessCategorySectorId the new value for businessCategorySectorId
     */
    public void setBusinessCategorySectorId(int aBusinessCategorySectorId) {
        businessCategorySectorId = aBusinessCategorySectorId;
    }

    /**
     * Access method for businessCategorySectorName.
     *
     * @return the current value of businessCategorySectorName
     */
    public String getBusinessCategorySectorName() {
        return businessCategorySectorName;
    }

    /**
     * Setter method for businessCategorySectorName.
     *
     * @param aBusinessCategorySectorName the new value for businessCategorySectorName
     */
    public void setBusinessCategorySectorName(String aBusinessCategorySectorName) {
        businessCategorySectorName = aBusinessCategorySectorName;
    }

    /**
     * Access method for storeName.
     *
     * @return the current value of storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * Setter method for storeName.
     *
     * @param aStoreName the new value for storeName
     */
    public void setStoreName(String aStoreName) {
        storeName = aStoreName;
    }

    /**
     * Access method for currency.
     *
     * @return the current value of currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Setter method for currency.
     *
     * @param aCurrency the new value for currency
     */
    public void setCurrency(String aCurrency) {
        currency = aCurrency;
    }

    /**
     * Access method for contactNumber.
     *
     * @return the current value of contactNumber
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     * Setter method for contactNumber.
     *
     * @param aContactNumber the new value for contactNumber
     */
    public void setContactNumber(String aContactNumber) {
        contactNumber = aContactNumber;
    }

    /**
     * Access method for localListingRange.
     *
     * @return the current value of localListingRange
     */
    public int getLocalListingRange() {
        return localListingRange;
    }

    /**
     * Setter method for localListingRange.
     *
     * @param aLocalListingRange the new value for localListingRange
     */
    public void setLocalListingRange(int aLocalListingRange) {
        localListingRange = aLocalListingRange;
    }

    /**
     * Access method for deliveryAttributes.
     *
     * @return the current value of deliveryAttributes
     */
    public String getDeliveryAttributes() {
        return deliveryAttributes;
    }

    /**
     * Setter method for deliveryAttributes.
     *
     * @param aDeliveryAttributes the new value for deliveryAttributes
     */
    public void setDeliveryAttributes(String aDeliveryAttributes) {
        deliveryAttributes = aDeliveryAttributes;
    }

    /**
     * Access method for storeOperation.
     *
     * @return the current value of storeOperation
     */
    public String getStoreOperation() {
        return storeOperation;
    }

    /**
     * Setter method for storeOperation.
     *
     * @param aStoreOperation the new value for storeOperation
     */
    public void setStoreOperation(String aStoreOperation) {
        storeOperation = aStoreOperation;
    }

    /**
     * Access method for businessHoursLimitationType.
     *
     * @return the current value of businessHoursLimitationType
     */
    public String getBusinessHoursLimitationType() {
        return businessHoursLimitationType;
    }

    /**
     * Setter method for businessHoursLimitationType.
     *
     * @param aBusinessHoursLimitationType the new value for businessHoursLimitationType
     */
    public void setBusinessHoursLimitationType(String aBusinessHoursLimitationType) {
        businessHoursLimitationType = aBusinessHoursLimitationType;
    }

    /**
     * Access method for locationKeyword.
     *
     * @return the current value of locationKeyword
     */
    public String getLocationKeyword() {
        return locationKeyword;
    }

    /**
     * Setter method for locationKeyword.
     *
     * @param aLocationKeyword the new value for locationKeyword
     */
    public void setLocationKeyword(String aLocationKeyword) {
        locationKeyword = aLocationKeyword;
    }

    /**
     * Access method for categoryKeyword.
     *
     * @return the current value of categoryKeyword
     */
    public String getCategoryKeyword() {
        return categoryKeyword;
    }

    /**
     * Setter method for categoryKeyword.
     *
     * @param aCategoryKeyword the new value for categoryKeyword
     */
    public void setCategoryKeyword(String aCategoryKeyword) {
        categoryKeyword = aCategoryKeyword;
    }

    /**
     * Access method for keywords.
     *
     * @return the current value of keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * Setter method for keywords.
     *
     * @param aKeywords the new value for keywords
     */
    public void setKeywords(String aKeywords) {
        keywords = aKeywords;
    }

    /**
     * Access method for isLive.
     *
     * @return true if and only if isLive is currently true
     */
    public boolean getIsLive() {
        return isLive;
    }

    /**
     * Setter method for isLive.
     *
     * @param aIsLive the new value for isLive
     */
    public void setIsLive(boolean aIsLive) {
        isLive = aIsLive;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(String aStatus) {
        status = aStatus;
    }

    /**
     * Access method for storeType.
     *
     * @return the current value of storeType
     */
    public String getStoreType() {
        return storeType;
    }

    /**
     * Setter method for storeType.
     *
     * @param aStoreType the new value for storeType
     */
    public void setStoreType(String aStoreType) {
        storeType = aStoreType;
    }

    /**
     * Access method for isBrandStore.
     *
     * @return true if and only if isBrandStore is currently true
     */
    public boolean getIsBrandStore() {
        return isBrandStore;
    }

    /**
     * Setter method for isBrandStore.
     *
     * @param aIsBrandStore the new value for isBrandStore
     */
    public void setIsBrandStore(boolean aIsBrandStore) {
        isBrandStore = aIsBrandStore;
    }

    /**
     * Access method for isVerifiedStore.
     *
     * @return true if and only if isVerifiedStore is currently true
     */
    public boolean getIsVerifiedStore() {
        return isVerifiedStore;
    }

    /**
     * Setter method for isVerifiedStore.
     *
     * @param aIsVerifiedStore the new value for isVerifiedStore
     */
    public void setIsVerifiedStore(boolean aIsVerifiedStore) {
        isVerifiedStore = aIsVerifiedStore;
    }

    /**
     * Access method for createdBy.
     *
     * @return the current value of createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter method for createdBy.
     *
     * @param aCreatedBy the new value for createdBy
     */
    public void setCreatedBy(String aCreatedBy) {
        createdBy = aCreatedBy;
    }

    /**
     * Access method for createdAt.
     *
     * @return the current value of createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * Setter method for createdAt.
     *
     * @param aCreatedAt the new value for createdAt
     */
    public void setCreatedAt(LocalDateTime aCreatedAt) {
        createdAt = aCreatedAt;
    }

    /**
     * Access method for updatedAt.
     *
     * @return the current value of updatedAt
     */
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Setter method for updatedAt.
     *
     * @param aUpdatedAt the new value for updatedAt
     */
    public void setUpdatedAt(LocalDateTime aUpdatedAt) {
        updatedAt = aUpdatedAt;
    }

    /**
     * Access method for updatedBy.
     *
     * @return the current value of updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Setter method for updatedBy.
     *
     * @param aUpdatedBy the new value for updatedBy
     */
    public void setUpdatedBy(String aUpdatedBy) {
        updatedBy = aUpdatedBy;
    }

    /**
     * Access method for allowMarketplace.
     *
     * @return true if and only if allowMarketplace is currently true
     */
    public boolean getAllowMarketplace() {
        return allowMarketplace;
    }

    /**
     * Setter method for allowMarketplace.
     *
     * @param aAllowMarketplace the new value for allowMarketplace
     */
    public void setAllowMarketplace(boolean aAllowMarketplace) {
        allowMarketplace = aAllowMarketplace;
    }

    /**
     * Access method for allowLocalListing.
     *
     * @return true if and only if allowLocalListing is currently true
     */
    public boolean getAllowLocalListing() {
        return allowLocalListing;
    }

    /**
     * Setter method for allowLocalListing.
     *
     * @param aAllowLocalListing the new value for allowLocalListing
     */
    public void setAllowLocalListing(boolean aAllowLocalListing) {
        allowLocalListing = aAllowLocalListing;
    }

    /**
     * Access method for promotionalKeywords.
     *
     * @return the current value of promotionalKeywords
     */
    public String getPromotionalKeywords() {
        return promotionalKeywords;
    }

    /**
     * Setter method for promotionalKeywords.
     *
     * @param aPromotionalKeywords the new value for promotionalKeywords
     */
    public void setPromotionalKeywords(String aPromotionalKeywords) {
        promotionalKeywords = aPromotionalKeywords;
    }

    /**
     * Access method for zoneId.
     *
     * @return the current value of zoneId
     */
    public int getZoneId() {
        return zoneId;
    }

    /**
     * Setter method for zoneId.
     *
     * @param aZoneId the new value for zoneId
     */
    public void setZoneId(int aZoneId) {
        zoneId = aZoneId;
    }

    /**
     * Access method for storeCoverImage.
     *
     * @return the current value of storeCoverImage
     */
    public String getStoreCoverImage() {
        return storeCoverImage;
    }

    /**
     * Setter method for storeCoverImage.
     *
     * @param aStoreCoverImage the new value for storeCoverImage
     */
    public void setStoreCoverImage(String aStoreCoverImage) {
        storeCoverImage = aStoreCoverImage;
    }

    /**
     * Access method for categoryIds.
     *
     * @return the current value of categoryIds
     */
    public String getCategoryIds() {
        return categoryIds;
    }

    /**
     * Setter method for categoryIds.
     *
     * @param aCategoryIds the new value for categoryIds
     */
    public void setCategoryIds(String aCategoryIds) {
        categoryIds = aCategoryIds;
    }

    /**
     * Access method for rejectUser.
     *
     * @return the current value of rejectUser
     */
    public String getRejectUser() {
        return rejectUser;
    }

    /**
     * Setter method for rejectUser.
     *
     * @param aRejectUser the new value for rejectUser
     */
    public void setRejectUser(String aRejectUser) {
        rejectUser = aRejectUser;
    }

    /**
     * Access method for rejectRemarks.
     *
     * @return the current value of rejectRemarks
     */
    public String getRejectRemarks() {
        return rejectRemarks;
    }

    /**
     * Setter method for rejectRemarks.
     *
     * @param aRejectRemarks the new value for rejectRemarks
     */
    public void setRejectRemarks(String aRejectRemarks) {
        rejectRemarks = aRejectRemarks;
    }

    /**
     * Access method for tagLine.
     *
     * @return the current value of tagLine
     */
    public String getTagLine() {
        return tagLine;
    }

    /**
     * Setter method for tagLine.
     *
     * @param aTagLine the new value for tagLine
     */
    public void setTagLine(String aTagLine) {
        tagLine = aTagLine;
    }

    /**
     * Access method for authorizedCertificates.
     *
     * @return the current value of authorizedCertificates
     */
    public String getAuthorizedCertificates() {
        return authorizedCertificates;
    }

    /**
     * Setter method for authorizedCertificates.
     *
     * @param aAuthorizedCertificates the new value for authorizedCertificates
     */
    public void setAuthorizedCertificates(String aAuthorizedCertificates) {
        authorizedCertificates = aAuthorizedCertificates;
    }

    /**
     * Access method for orderProcessed.
     *
     * @return the current value of orderProcessed
     */
    public int getOrderProcessed() {
        return orderProcessed;
    }

    /**
     * Setter method for orderProcessed.
     *
     * @param aOrderProcessed the new value for orderProcessed
     */
    public void setOrderProcessed(int aOrderProcessed) {
        orderProcessed = aOrderProcessed;
    }

    /**
     * Access method for paymentOptions.
     *
     * @return the current value of paymentOptions
     */
    public String getPaymentOptions() {
        return paymentOptions;
    }

    /**
     * Setter method for paymentOptions.
     *
     * @param aPaymentOptions the new value for paymentOptions
     */
    public void setPaymentOptions(String aPaymentOptions) {
        paymentOptions = aPaymentOptions;
    }

    /**
     * Access method for cancelReturnOptions.
     *
     * @return the current value of cancelReturnOptions
     */
    public String getCancelReturnOptions() {
        return cancelReturnOptions;
    }

    /**
     * Setter method for cancelReturnOptions.
     *
     * @param aCancelReturnOptions the new value for cancelReturnOptions
     */
    public void setCancelReturnOptions(String aCancelReturnOptions) {
        cancelReturnOptions = aCancelReturnOptions;
    }

    /**
     * Access method for allowScheduledDelivery.
     *
     * @return true if and only if allowScheduledDelivery is currently true
     */
    public boolean getAllowScheduledDelivery() {
        return allowScheduledDelivery;
    }

    /**
     * Setter method for allowScheduledDelivery.
     *
     * @param aAllowScheduledDelivery the new value for allowScheduledDelivery
     */
    public void setAllowScheduledDelivery(boolean aAllowScheduledDelivery) {
        allowScheduledDelivery = aAllowScheduledDelivery;
    }

    /**
     * Access method for scheduledDeliveryLengthMinute.
     *
     * @return the current value of scheduledDeliveryLengthMinute
     */
    public int getScheduledDeliveryLengthMinute() {
        return scheduledDeliveryLengthMinute;
    }

    /**
     * Setter method for scheduledDeliveryLengthMinute.
     *
     * @param aScheduledDeliveryLengthMinute the new value for scheduledDeliveryLengthMinute
     */
    public void setScheduledDeliveryLengthMinute(int aScheduledDeliveryLengthMinute) {
        scheduledDeliveryLengthMinute = aScheduledDeliveryLengthMinute;
    }

    /**
     * Access method for additionalPackagingCharge.
     *
     * @return the current value of additionalPackagingCharge
     */
    public double getAdditionalPackagingCharge() {
        return additionalPackagingCharge;
    }

    /**
     * Setter method for additionalPackagingCharge.
     *
     * @param aAdditionalPackagingCharge the new value for additionalPackagingCharge
     */
    public void setAdditionalPackagingCharge(double aAdditionalPackagingCharge) {
        additionalPackagingCharge = aAdditionalPackagingCharge;
    }

    /**
     * Access method for packagingChargeApplicable.
     *
     * @return the current value of packagingChargeApplicable
     */
    public String getPackagingChargeApplicable() {
        return packagingChargeApplicable;
    }

    /**
     * Setter method for packagingChargeApplicable.
     *
     * @param aPackagingChargeApplicable the new value for packagingChargeApplicable
     */
    public void setPackagingChargeApplicable(String aPackagingChargeApplicable) {
        packagingChargeApplicable = aPackagingChargeApplicable;
    }

    /**
     * Access method for taxApplicable.
     *
     * @return the current value of taxApplicable
     */
    public String getTaxApplicable() {
        return taxApplicable;
    }

    /**
     * Setter method for taxApplicable.
     *
     * @param aTaxApplicable the new value for taxApplicable
     */
    public void setTaxApplicable(String aTaxApplicable) {
        taxApplicable = aTaxApplicable;
    }

    /**
     * Access method for taxPercentage.
     *
     * @return the current value of taxPercentage
     */
    public double getTaxPercentage() {
        return taxPercentage;
    }

    /**
     * Setter method for taxPercentage.
     *
     * @param aTaxPercentage the new value for taxPercentage
     */
    public void setTaxPercentage(double aTaxPercentage) {
        taxPercentage = aTaxPercentage;
    }

    /**
     * Access method for customOrder.
     *
     * @return true if and only if customOrder is currently true
     */
    public boolean getCustomOrder() {
        return customOrder;
    }

    /**
     * Setter method for customOrder.
     *
     * @param aCustomOrder the new value for customOrder
     */
    public void setCustomOrder(boolean aCustomOrder) {
        customOrder = aCustomOrder;
    }

    /**
     * Access method for posEnabled.
     *
     * @return true if and only if posEnabled is currently true
     */
    public boolean getPosEnabled() {
        return posEnabled;
    }

    /**
     * Setter method for posEnabled.
     *
     * @param aPosEnabled the new value for posEnabled
     */
    public void setPosEnabled(boolean aPosEnabled) {
        posEnabled = aPosEnabled;
    }

    /**
     * Compares the key for this instance with another MerchantStores.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MerchantStores and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MerchantStores)) {
            return false;
        }
        MerchantStores that = (MerchantStores) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MerchantStores.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MerchantStores)) return false;
        return this.equalKeys(other) && ((MerchantStores)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MerchantStores |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
